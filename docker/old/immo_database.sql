
SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema immo_database
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `immo_database` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci ;
USE `immo_database` ;

-- -----------------------------------------------------
-- Table `immo_database`.`real_estate_agencies`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `immo_database`.`real_estate_agencies` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NOT NULL,
  `users_id` INT UNSIGNED NOT NULL,
  INDEX `users_id_idx` (`users_id` ASC),
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `immo_database`.`users`
-- -----------------------------------------------------

CREATE TABLE IF NOT EXISTS `immo_database`.`users` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `username` VARCHAR(45) NOT NULL,
  `last_name` VARCHAR(45) NOT NULL,
  `first_name` VARCHAR(45) NOT NULL,
  `email` VARCHAR(100) NOT NULL,
  `phonenumber` VARCHAR(15) NOT NULL,
  `password` VARCHAR(255) NOT NULL,
  `role` ENUM("admin", "agent", "client") NOT NULL,
  `real_estate_agencies_id` INT UNSIGNED NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `email_UNIQUE` (`email` ASC),
  UNIQUE INDEX `username_UNIQUE` (`username` ASC),
  INDEX `fk_users_real_estate_agencies1_idx` (`real_estate_agencies_id` ASC),
  CONSTRAINT `fk_users_real_estate_agencies1`
    FOREIGN KEY (`real_estate_agencies_id`)
    REFERENCES `immo_database`.`real_estate_agencies` (`users_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

INSERT INTO `users` (`id`, `username`, `last_name`, `first_name`, `email`, `phonenumber`, `password`, `role`) VALUES
(1, 'Mango', 'Meuleman', 'Margaux', 'margaux.meuleman@student.odisee.be', '041234567890', '$2y$10$mtwGKw.1MGGqXRSUtTsdR.m7N7MyBc/5ZDomxShYcxfThkhvJSWMK', 'admin');


-- -----------------------------------------------------
-- Table `immo_database`.`residences`
-- -----------------------------------------------------

CREATE TABLE IF NOT EXISTS `immo_database`.`residences` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `type` ENUM("Apartment", "Cottage", "Penthouse", "Mansion", "Single-family", "Multi-family", "Bungalow", "Townhouse") NULL,
  `price` DECIMAL(10,2) NOT NULL,
  `name` VARCHAR(100) NOT NULL,
  `description` TEXT NOT NULL,
  `city` VARCHAR(45) NOT NULL,
  `country` VARCHAR(45) NOT NULL,
  `zip_code` VARCHAR(10) NOT NULL,
  `user_id` INT UNSIGNED NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_residences_users1_idx` (`user_id` ASC),
  CONSTRAINT `fk_residences_users1`
    FOREIGN KEY (`user_id`)
    REFERENCES `immo_database`.`users` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `immo_database`.`parcels`
-- -----------------------------------------------------

CREATE TABLE IF NOT EXISTS `immo_database`.`parcels` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NOT NULL,
  `length` INT NOT NULL,
  `width` INT NOT NULL,
  `price` DECIMAL(10,2) NOT NULL,
  `description` TEXT NOT NULL,
  `city` VARCHAR(45) NOT NULL,
  `country` VARCHAR(45) NOT NULL,
  `zip_code` VARCHAR(10) NOT NULL,
  `user_id` INT UNSIGNED NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_parcels_users1_idx` (`user_id` ASC),
  CONSTRAINT `fk_parcels_users1`
    FOREIGN KEY (`user_id`)
    REFERENCES `immo_database`.`users` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `immo_database`.`example properties`
-- -----------------------------------------------------

CREATE TABLE IF NOT EXISTS `immo_database`.`example properties` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `type` ENUM("apartment") NOT NULL,
  `surface` INT UNSIGNED NOT NULL,
  `bathrooms` TINYINT UNSIGNED NOT NULL,
  `bedrooms` TINYINT UNSIGNED NOT NULL,
  `garage` TINYINT(1) UNSIGNED NULL,
  `garden_size` INT NULL,
  `basement` TINYINT(1) NULL,
  `attic` TINYINT(1) NULL,
  `living_room` TINYINT(1) NULL,
  `dining_room` TINYINT(1) NULL,
  `dressing` TINYINT(1) NULL,
  `laundry_room` TINYINT(1) NULL,
  `cellar` TINYINT(1) NULL,
  `city` VARCHAR(45) NULL,
  `country` VARCHAR(45) NULL,
  `zip_code` VARCHAR(10) NULL,
  `roof_type` VARCHAR(45) NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `immo_database`.`user_has_parcel`
-- -----------------------------------------------------

CREATE TABLE IF NOT EXISTS `immo_database`.`user_has_parcel` (
  `user_id` INT UNSIGNED NOT NULL,
  `parcel_id` INT UNSIGNED NOT NULL,
  PRIMARY KEY (`user_id`, `parcel_id`),
  INDEX `fk_users_has_parcels_parcels1_idx` (`parcel_id` ASC),
  INDEX `fk_users_has_parcels_users1_idx` (`user_id` ASC),
  CONSTRAINT `fk_users_has_parcels_users1`
    FOREIGN KEY (`user_id`)
    REFERENCES `immo_database`.`users` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_users_has_parcels_parcels1`
    FOREIGN KEY (`parcel_id`)
    REFERENCES `immo_database`.`parcels` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `immo_database`.`user_has_residence`
-- -----------------------------------------------------

CREATE TABLE IF NOT EXISTS `immo_database`.`user_has_residence` (
  `user_id` INT UNSIGNED NOT NULL,
  `residence_id` INT UNSIGNED NOT NULL,
  PRIMARY KEY (`user_id`, `residence_id`),
  INDEX `fk_users_has_residences_residences1_idx` (`residence_id` ASC),
  INDEX `fk_users_has_residences_users1_idx` (`user_id` ASC),
  CONSTRAINT `fk_users_has_residences_users1`
    FOREIGN KEY (`user_id`)
    REFERENCES `immo_database`.`users` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_users_has_residences_residences1`
    FOREIGN KEY (`residence_id`)
    REFERENCES `immo_database`.`residences` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `immo_database`.`filters`
-- -----------------------------------------------------

CREATE TABLE IF NOT EXISTS `immo_database`.`filters` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `immo_database`.`user_has_filter`
-- -----------------------------------------------------

CREATE TABLE IF NOT EXISTS `immo_database`.`user_has_filter` (
  `user_id` INT UNSIGNED NOT NULL,
  `filter_idfilters` INT UNSIGNED NOT NULL,
  PRIMARY KEY (`user_id`, `filter_idfilters`),
  INDEX `fk_users_has_filters_filters1_idx` (`filter_idfilters` ASC),
  INDEX `fk_users_has_filters_users1_idx` (`user_id` ASC),
  CONSTRAINT `fk_users_has_filters_users1`
    FOREIGN KEY (`user_id`)
    REFERENCES `immo_database`.`users` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_users_has_filters_filters1`
    FOREIGN KEY (`filter_idfilters`)
    REFERENCES `immo_database`.`filters` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `immo_database`.`properties`
-- -----------------------------------------------------

CREATE TABLE IF NOT EXISTS `immo_database`.`properties` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NOT NULL,
  `count` TINYINT NULL,
  `width` DECIMAL(4,2) NULL,
  `length` DECIMAL(4,2) NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `immo_database`.`residence_has_property`
-- -----------------------------------------------------

CREATE TABLE IF NOT EXISTS `immo_database`.`residence_has_property` (
  `residences_id` INT UNSIGNED NOT NULL,
  `properties_id` INT UNSIGNED NOT NULL,
  PRIMARY KEY (`residences_id`, `properties_id`),
  INDEX `fk_residences_has_properties_properties1_idx` (`properties_id` ASC),
  INDEX `fk_residences_has_properties_residences1_idx` (`residences_id` ASC),
  CONSTRAINT `fk_residences_has_properties_residences1`
    FOREIGN KEY (`residences_id`)
    REFERENCES `immo_database`.`residences` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_residences_has_properties_properties1`
    FOREIGN KEY (`properties_id`)
    REFERENCES `immo_database`.`properties` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;

