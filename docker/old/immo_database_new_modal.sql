-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema immo_database_new
-- -----------------------------------------------------
DROP SCHEMA IF EXISTS `immo_database_new` ;

-- -----------------------------------------------------
-- Schema immo_database_new
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `immo_database_new` DEFAULT CHARACTER SET utf8 ;
USE `immo_database_new` ;

-- -----------------------------------------------------
-- Table `immo_database_new`.`estate_agencies`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `immo_database_new`.`estate_agencies` ;

CREATE TABLE IF NOT EXISTS `immo_database_new`.`estate_agencies` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `immo_database_new`.`users`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `immo_database_new`.`users` ;

CREATE TABLE IF NOT EXISTS `immo_database_new`.`users` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `username` VARCHAR(45) NOT NULL,
  `email` VARCHAR(100) NOT NULL,
  `name` VARCHAR(45) NOT NULL,
  `phone_number` VARCHAR(15) NOT NULL,
  `password` VARCHAR(255) NOT NULL,
  `role` ENUM('admin', 'agent', 'client') NOT NULL,
  `estate_agencies_id` INT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_users_estate_agencies1_idx` (`estate_agencies_id` ASC) VISIBLE,
  CONSTRAINT `fk_users_estate_agencies1`
    FOREIGN KEY (`estate_agencies_id`)
    REFERENCES `immo_database_new`.`estate_agencies` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `immo_database_new`.`properties`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `immo_database_new`.`properties` ;

CREATE TABLE IF NOT EXISTS `immo_database_new`.`properties` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `bedrooms` TINYINT NOT NULL,
  `bathrooms` TINYINT NOT NULL,
  `garages` TINYINT NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `immo_database_new`.`addresses`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `immo_database_new`.`addresses` ;

CREATE TABLE IF NOT EXISTS `immo_database_new`.`addresses` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `country` VARCHAR(45) NOT NULL,
  `city` VARCHAR(45) NOT NULL,
  `zip_code` SMALLINT NOT NULL,
  `street` VARCHAR(100) NOT NULL,
  `street_number` SMALLINT NOT NULL,
  `street_bus` VARCHAR(2) NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `immo_database_new`.`residences`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `immo_database_new`.`residences` ;

CREATE TABLE IF NOT EXISTS `immo_database_new`.`residences` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `type` ENUM('Apartment', 'Cottage', 'Penthouse', 'Mansion', 'Single-family', 'Multi-family', 'Bungalow', 'Townhouse', 'ground') NOT NULL,
  `description` VARCHAR(255) NULL,
  `price` DECIMAL(10,2) NOT NULL,
  `surface` SMALLINT NULL,
  `properties_id` INT NOT NULL,
  `addresses_id` INT NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_residences_properties_idx` (`properties_id` ASC) VISIBLE,
  INDEX `fk_residences_addresses1_idx` (`addresses_id` ASC) VISIBLE,
  CONSTRAINT `fk_residences_properties`
    FOREIGN KEY (`properties_id`)
    REFERENCES `immo_database_new`.`properties` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_residences_addresses1`
    FOREIGN KEY (`addresses_id`)
    REFERENCES `immo_database_new`.`addresses` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `immo_database_new`.`user_favourites`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `immo_database_new`.`user_favourites` ;

CREATE TABLE IF NOT EXISTS `immo_database_new`.`user_favourites` (
  `users_id` INT NOT NULL,
  `residences_id` INT NOT NULL,
  PRIMARY KEY (`users_id`, `residences_id`),
  INDEX `fk_users_has_residences_residences1_idx` (`residences_id` ASC) VISIBLE,
  INDEX `fk_users_has_residences_users1_idx` (`users_id` ASC) VISIBLE,
  CONSTRAINT `fk_users_has_residences_users1`
    FOREIGN KEY (`users_id`)
    REFERENCES `immo_database_new`.`users` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_users_has_residences_residences1`
    FOREIGN KEY (`residences_id`)
    REFERENCES `immo_database_new`.`residences` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `immo_database_new`.`users_preferences`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `immo_database_new`.`users_preferences` ;

CREATE TABLE IF NOT EXISTS `immo_database_new`.`users_preferences` (
  `users_id` INT NOT NULL,
  `properties_id` INT NOT NULL,
  PRIMARY KEY (`users_id`, `properties_id`),
  INDEX `fk_users_has_properties_properties1_idx` (`properties_id` ASC) VISIBLE,
  INDEX `fk_users_has_properties_users1_idx` (`users_id` ASC) VISIBLE,
  CONSTRAINT `fk_users_has_properties_users1`
    FOREIGN KEY (`users_id`)
    REFERENCES `immo_database_new`.`users` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_users_has_properties_properties1`
    FOREIGN KEY (`properties_id`)
    REFERENCES `immo_database_new`.`properties` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
