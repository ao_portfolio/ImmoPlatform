-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: mysqldb
-- Gegenereerd op: 16 jan 2022 om 18:30
-- Serverversie: 5.7.35
-- PHP-versie: 7.4.20

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+01:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `immo_database_new`
--
CREATE DATABASE  IF NOT EXISTS `immo_database_new` /*!40100 DEFAULT CHARACTER SET utf8 */ /*!80016 DEFAULT ENCRYPTION='N' */;
USE `immo_database_new`;
-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `addresses`
--

CREATE TABLE `addresses` (
  `id` int(11) NOT NULL,
  `country` varchar(45) NOT NULL,
  `city` varchar(45) NOT NULL,
  `zip_code` smallint(6) NOT NULL,
  `street` varchar(100) NOT NULL,
  `street_number` smallint(6) NOT NULL,
  `street_bus` varchar(2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Gegevens worden geëxporteerd voor tabel `addresses`
--

INSERT INTO `addresses` (`id`, `country`, `city`, `zip_code`, `street`, `street_number`, `street_bus`) VALUES
(1, 'Belgium', 'Gent', 9000, 'Driesstraat', 99, '12'),
(2, 'Belgium', 'Antwerpen', 5000, 'Dambruggestraat', 21, NULL),
(3, 'Belgium', 'Antwerpen', 5000, 'Van Arteveldestraat', 37, NULL),
(4, 'Belgium', 'Lasne', 1380, 'Linieweg', 389, NULL),
(5, 'Belgium', 'Mol', 2400, 'Luchthavenlaan', 481, NULL),
(6, 'Belgium', 'Kieldrect', 9130, 'Maskenstraat', 490, NULL),
(7, 'Belgium', 'Xhoris', 4190, 'Rue des Honnelles', 291, NULL),
(8, 'Belgium', 'Zuidschote', 8904, 'Heuvenstraat', 216, NULL),
(9, 'Belgium', 'Bevere', 9700, 'Kerkstraat', 3, NULL),
(10, 'Belgium', 'Niel', 2845, 'Passiewijk', 471, NULL),
(11, 'Belgium', 'Nieuwkerke', 8950, 'Alsembergsesteenweg', 426, NULL),
(12, 'Belgium', 'Awenne', 6870, 'Hoge Wei', 268, NULL),
(13, 'Belgium', 'Zingem', 9750, 'Touwlagerstraat', 23, NULL),
(14, 'België', 'Zelzate', 9060, 'Assenedesteenweg', 45, '');

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `estate_agencies`
--

CREATE TABLE `estate_agencies` (
  `id` int(11) NOT NULL,
  `name` varchar(45) NOT NULL,
  `blocked` tinyint(4) NOT NULL DEFAULT '0',
  `addresses_id` int(11) DEFAULT NULL,
  `images` varchar(1024) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Gegevens worden geëxporteerd voor tabel `estate_agencies`
--

INSERT INTO `estate_agencies` (`id`, `name`, `blocked`, `addresses_id`, `images`) VALUES
(1, 'Immo Da Vinci', 0, 14, '1.jpg'),
(2, 'Berno Dendermonde', 0, 14, '1.jpg'),
(3, 'Carlo Eggermont', 0, 14, '1.jpg'),
(4, 'Woonservice VDV', 0, 14, '1.jpg'),
(5, 'Affinimmo', 0, 14, '1.jpg'),
(6, 'CPlus Vastgoed', 0, 14, '1.jpg'),
(7, 'NB-Projects', 0, 14, '1.jpg'),
(8, 'Convas', 0, 14, '1.jpg'),
(9, 'Stellar Immo', 0, 14, '1.jpg'),
(10, 'KMO Vastgoed', 0, 14, '1.jpg'),
(11, 'Agence Rosseel', 0, 14, '1.jpg'),
(12, 'Bostoen', 0, 14, '1.jpg'),
(13, 'Acasa', 0, 14, '1.jpg'),
(14, 'React Gent', 0, 14, '1.jpg'),
(15, 'Urbanlink', 0, 14, '1.jpg'),
(16, 'Maximm', 0, 14, '1.jpg'),
(17, 'Crevits', 0, 14, '1.jpg'),
(18, 'Trevi Gent', 0, 14, '1.jpg'),
(19, 'Immobert', 0, 14, '1.jpg'),
(20, 'Sofimmo', 0, 14, '1.jpg');

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `properties`
--

CREATE TABLE `properties` (
  `id` int(11) NOT NULL,
  `bedrooms` tinyint(4) NOT NULL,
  `bathrooms` tinyint(4) NOT NULL,
  `garages` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Gegevens worden geëxporteerd voor tabel `properties`
--

INSERT INTO `properties` (`id`, `bedrooms`, `bathrooms`, `garages`) VALUES
(1, 1, 1, 0),
(2, 1, 1, 1),
(3, 1, 2, 1),
(4, 2, 1, 0),
(5, 2, 1, 1),
(6, 2, 2, 1),
(7, 2, 2, 2),
(8, 3, 1, 0),
(9, 3, 1, 1),
(10, 3, 2, 1),
(11, 3, 2, 2),
(12, 4, 2, 1),
(13, 4, 3, 2),
(14, 5, 3, 2),
(15, 8, 5, 6),
(16, 10, 10, 20),
(17, 15, 12, 15),
(18, 20, 15, 40),
(19, 0, 0, 0),
(20, 69, 0, 0),
(21, 69, 0, 0),
(22, 69, 0, 0),
(23, 69, 0, 0),
(24, 69, 0, 0),
(25, 69, 0, 0),
(26, 69, 0, 0),
(27, 69, 0, 0);

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `residences`
--

CREATE TABLE `residences` (
  `id` int(11) NOT NULL,
  `type` enum('Apartment','Cottage','Penthouse','Mansion','Single-family','Multi-family','Bungalow','Townhouse','ground') NOT NULL,
  `description` TEXT DEFAULT NULL,
  `price` decimal(10,2) NOT NULL,
  `payment` enum('For sale','For rent') NOT NULL,
  `surface` smallint(6) DEFAULT NULL,
  `images` varchar(1024) DEFAULT NULL,
  `energy_label` enum('A+++','A++','A+','A','B','C','D','E','F') NOT NULL,
  `date_added` DATETIME NOT NULL DEFAULT NOW(),
  `properties_id` int(11) NOT NULL,
  `addresses_id` int(11) NOT NULL,
  `estate_agencies_id` int(11) NOT NULL,
  `featured` tinyint(4) NOT NULL DEFAULT '0',
  `sold` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Gegevens worden geëxporteerd voor tabel `residences`
--

INSERT INTO `residences` (`id`, `type`, `description`, `price`, `payment`, `surface`, `images`, `energy_label`, `date_added`, `properties_id`, `addresses_id`, `estate_agencies_id`, `featured`, `sold`) VALUES
(1, 'Single-family', NULL, '240000.00', 'For sale', 180, '1.jpg,2.jpg,3.jpg', 'B', '2021-12-29', 9, 2, 18, 0, 0),
(2, 'Multi-family', NULL, '379000.00', 'For sale', 250, '1.jpg,2.jpg,3.jpg', 'A+', '2021-12-29', 12, 3, 19, 0, 0),
(3, 'Apartment', NULL, '419000.00', 'For sale', 333, '1.jpg,2.jpg,3.jpg', 'A', '2022-01-03', 14, 1, 7, 0, 0),
(4, 'Single-family', NULL, '129000.00', 'For sale', 116, '1.jpg,2.jpg,3.jpg', 'C', '2022-01-10', 8, 8, 11, 0, 0),
(5, 'Multi-family', NULL, '449000.00', 'For sale', 228, '1.jpg,2.jpg,3.jpg', 'A++', '2022-01-10', 14, 9, 16, 0, 0),
(6, 'Mansion', NULL, '990000.00', 'For sale', 485, '1.jpg,2.jpg,3.jpg', 'D', '2022-01-12', 15, 13, 12, 0, 0),
(7, 'Multi-family', NULL, '5250000.00', 'For sale', 368, '1.jpg,2.jpg,3.jpg', 'A', '2022-01-12', 14, 10, 15, 0, 0),
(8, 'Cottage', NULL, '1000000.00', 'For sale', 260, '1.jpg,2.jpg,3.jpg', 'A++', '2022-01-13', 15, 11, 12, 0, 0),
(9, 'Townhouse', NULL, '399000.00', 'For sale', 150, '1.jpg,2.jpg,3.jpg', 'B', '2022-01-15', 13, 12, 9, 0, 0),
(10, 'Bungalow', NULL, '995000.00', 'For sale', 380, '1.jpg,2.jpg,3.jpg', 'A++', '2022-01-15', 13, 6, 3, 0, 0),
(11, 'Penthouse', NULL, '4850000.00', 'For sale', 165, '1.jpg,2.jpg,3.jpg', 'A+++', '2022-01-15', 10, 4, 16, 0, 0),
(12, 'ground', NULL, '80000.00', 'For sale', 380, '1.jpg,2.jpg,3.jpg', 'A+++', '2022-01-16', 19, 5, 2, 0, 0),
(13, 'Single-family', NULL, '425.00', 'For rent', 91, '1.jpg,2.jpg,3.jpg', 'E', '2022-01-16', 4, 7, 2, 0, 0),
(14, 'Apartment', 'test', '10.00', 'For sale', NULL, '27.jpg', 'A+++', '2022-01-17', 27, 18, 1, 0, 0);

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `username` varchar(45) NOT NULL,
  `images` varchar(1024) NOT NULL,
  `email` varchar(100) NOT NULL,
  `name` varchar(45) NOT NULL,
  `phone_number` varchar(15) NOT NULL,
  `password` varchar(255) NOT NULL,
  `role` enum('admin','agent','client') NOT NULL,
  `estate_agencies_id` int(11) DEFAULT NULL,
  `added_on` DATETIME NOT NULL DEFAULT NOW()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Gegevens worden geëxporteerd voor tabel `users`
--

INSERT INTO `users` (`id`, `username`, `images`, `email`, `name`, `phone_number`, `password`, `role`, `estate_agencies_id`, `added_on`) VALUES
(1, 'silverswan131', '1.jpg', 'brad.gibson@example.com', 'Brad Gibson', '011-962-7516', '$2y$10$her1oEJ0R/w44WonHIWsb.fGf6jGHeumn3WF2YmS.OtBP3DiQ0KNS', 'client', NULL, '2022-01-06 12:07:01'),
(2, 'connieSimp046', '1.jpg', 'connie.simpson@example.com', 'Connie Simpson', '046-108-4740', '$2y$10$her1oEJ0R/w44WonHIWsb.fGf6jGHeumn3WF2YmS.OtBP3DiQ0KNS', 'agent', 11, '2022-01-07 18:18:56'),
(3, 'BobaFett', '1.jpg', 'bob.elliott@example.com', 'Bob Elliott', '414-560-3795', '$2y$10$her1oEJ0R/w44WonHIWsb.fGf6jGHeumn3WF2YmS.OtBP3DiQ0KNS', 'admin', NULL, '2022-01-05 00:08:53'),
(4, 'RandallStephy', '1.jpg', 'randall.stephens@example.com', 'Randall Stephens', '224-279-4934', '$2y$10$her1oEJ0R/w44WonHIWsb.fGf6jGHeumn3WF2YmS.OtBP3DiQ0KNS', 'agent', 8, '2022-01-01 20:36:32');

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `users_preferences`
--

CREATE TABLE `user_preferences` (
   `id` int(11) NOT NULL,
   `users_id` int(11) NOT NULL,
   `preferences` JSON NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Gegevens worden geëxporteerd voor tabel `users_preferences`
--

INSERT INTO `user_preferences` (`id`, `users_id`, `preferences`) VALUES
(1, 1, '{"keyword":"","type":"Single-family","city":"Antwerpen","bedrooms":"2","bathrooms":"1","garages":"0","min-price":"0","max-price":"750000"}'),
(2, 1, '{"keyword":"","type":"Townhouse","city":"Antwerpen","bedrooms":"3","bathrooms":"2","garages":"1","min-price":"0","max-price":"450000"}'),
(3, 4, '{"keyword":"","type":"Multi-family","city":"Gent","bedrooms":"4","bathrooms":"2","garages":"2","min-price":"500000","max-price":"1000000"}'),
(4, 2, '{"keyword":"","type":"Single-family","city":"Antwerpen","bedrooms":"3","bathrooms":"2","garages":"1","min-price":"250000","max-price":"500000"}');


-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `user_favourites`
--

CREATE TABLE `user_favourites` (
  `users_id` int(11) NOT NULL,
  `residences_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Gegevens worden geëxporteerd voor tabel `user_favourites`
--

INSERT INTO `user_favourites` (`users_id`, `residences_id`) VALUES
(1, 2),
(2, 5),
(1, 8),
(1, 10);

--
-- Indexen voor geëxporteerde tabellen
--

--
-- Indexen voor tabel `addresses`
--
ALTER TABLE `addresses`
  ADD PRIMARY KEY (`id`);

--
-- Indexen voor tabel `estate_agencies`
--
ALTER TABLE `estate_agencies`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_addresses_id_estate_agencies` (`addresses_id`);

--
-- Indexen voor tabel `properties`
--
ALTER TABLE `properties`
  ADD PRIMARY KEY (`id`);

--
-- Indexen voor tabel `residences`
--
ALTER TABLE `residences`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_residences_properties_idx` (`properties_id`),
  ADD KEY `fk_residences_addresses1_idx` (`addresses_id`),
  ADD KEY `fk_residences_estate_agencies1_idx` (`estate_agencies_id`);

--
-- Indexen voor tabel `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_users_estate_agencies1_idx` (`estate_agencies_id`);

--
-- Indexen voor tabel `users_preferences`
--
ALTER TABLE `user_preferences`
    ADD PRIMARY KEY (`id`),
  ADD KEY `fk_user_preferences_users1_idx` (`users_id`);


--
-- Indexen voor tabel `user_favourites`
--
ALTER TABLE `user_favourites`
  ADD PRIMARY KEY (`users_id`,`residences_id`),
  ADD KEY `fk_users_has_residences_residences1_idx` (`residences_id`),
  ADD KEY `fk_users_has_residences_users1_idx` (`users_id`);

--
-- AUTO_INCREMENT voor geëxporteerde tabellen
--

--
-- AUTO_INCREMENT voor een tabel `addresses`
--
ALTER TABLE `addresses`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT voor een tabel `estate_agencies`
--
ALTER TABLE `estate_agencies`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT voor een tabel `properties`
--
ALTER TABLE `properties`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT voor een tabel `residences`
--
ALTER TABLE `residences`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT voor een tabel `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
ALTER TABLE `user_preferences`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;


--
-- Beperkingen voor tabel `estate_agencies`
--
ALTER TABLE `estate_agencies`
  ADD CONSTRAINT `fk_addresses_id_estate_agencies` FOREIGN KEY (`addresses_id`) REFERENCES `addresses` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
