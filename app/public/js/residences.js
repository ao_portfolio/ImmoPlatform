// Source: https://developer.mozilla.org/en-US/docs/Web/API/URLSearchParams/set
let url = new URL(window.location.href);
let params = new URLSearchParams(url.search);

let filter = document.getElementById('filter');

filter.addEventListener('change', () => {
    let param;
    switch (parseInt(filter.value)) {
        case 1: param = 'New to Old'; break;
        case 2: param = 'For Rent'; break;
        case 3: param = 'For Sale'; break;
        default: param = 'All'; break;
    }   
    params.set('filter', param);
    console.log('' + params);
    window.location.href = '/residences/search/page/1?' + params;
});