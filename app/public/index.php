<?php

require_once('../vendor/autoload.php');

$router = new \Bramus\Router\Router();
$router->setNamespace('Http');

$router->before('GET|POST', '/.*', function () use ($router) {
	session_start();
});

$router->get('/', 'PropertiesController@index');

$router ->mount('/residences', function() use ($router) {
	$router->get('/page/(\d+)', 'PropertiesController@overview');
	$router->get('/favourites/page/(\d+)', 'PropertiesController@favourites');
	$router->get('/search/page/(\d+)(.*)', 'PropertiesController@search');
	$router->get('/(\d+)', 'PropertiesController@view');
	$router->get('/preferences/save(.*)', 'PropertiesController@savePreference');
	$router->get('/preferences/delete/(\d+)', 'PropertiesController@deletePreference');
	$router->get('/preferences', 'PropertiesController@viewPreferences');

	$router->post('/(\d+)/like', 'PropertiesController@like');
	$router->post('/(\d+)/unlike', 'PropertiesController@unlike');
});

//AccountController
$router->get('/login', 'AccountController@login');
$router->post('/login', 'AccountController@login');

$router->get('/logout', 'AccountController@logout');

$router->get('/registration', 'AccountController@register');
$router->post('/registration', 'AccountController@register');

//AgencyController
$router->get('/agency/(\d+)/edit', 'AgencyController@getAgencyForEdit');
$router->post('/agency/(\d+)/edit', 'AgencyController@editAgency');

$router->get('/agency/(\d+)', 'AgencyController@agencyPage');

$router->get('/account', 'AgencyController@account');
$router->get('/agents', 'AccountController@getAccounts');

$router->get('/agent/add', 'AccountController@getAgent');
$router->post('/agent/add', 'AccountController@agentCreate');

$router->get('/residences/add', 'PropertiesController@add');
$router->post('/residences/add', 'PropertiesController@add');

$router->get('/residences/(\d+)/edit', 'PropertiesController@edit');
$router->post('/residences/(\d+)/edit', 'PropertiesController@edit');

$router->mount('/dashboard', function() use ($router) {
	$router->get('/', 'DashboardController@index');
	$router->get('/agency/ranking/(\w+)', 'DashboardController@rankAgencies');
	$router->get('/agency/search', 'DashboardController@searchAgency');
	$router->get('/agency/add', 'AgencyController@getAgency');
	$router->post('/agency/add', 'AgencyController@agencyCreate');
	$router->get('/agency/blocked', 'DashboardController@blockedAgencies');
	$router->get('/residence/ranking', 'DashboardController@rankResidences');

	$router->post('/agency/(\d+)/block', 'DashboardController@blockAgency');
	$router->post('/agency/(\d+)/unblock', 'DashboardController@unblockAgency');
	$router->post('/agency/(\d+)/delete', 'DashboardController@removeAgency');
});

$router->get('/about', 'PropertiesController@about');

$router->get('/contact', 'PropertiesController@contact');

$router->set404(function() use ($router) {
	header('Location: /');
	exit();
});

$router->run();
?>