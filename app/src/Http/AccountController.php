<?php

namespace Http;

use SplFileInfo;

class AccountController extends BaseController
{
    public function login()
    {
        if (!empty($_SESSION['id'])){
            header('Location: /');
            exit();
        }
        $connection = $this->connection;
        $formErrors = []; // The encountered form errors

        $user = isset($_POST['username']) ? $_POST['username'] : '';
        $password = isset($_POST['pass']) ? $_POST['pass'] : '';

        if (isset($_POST['moduleAction']) && ($_POST['moduleAction'] == 'login')) {
            //check parameters
            $stmt = $connection->prepare('SELECT EXISTS(SELECT id FROM users WHERE username = ?);');
            $result = $stmt->executeQuery([$user]);
            $exists = $result->fetchAllAssociative();
            $exists = (boolean)$exists[0][array_key_first($exists[0])];

            //password check incomplete
            $stmt = $connection->prepare('SELECT password FROM users WHERE username = ?;');
            $result = $stmt->executeQuery([$_POST['username']]);
            $resultArray = $result->fetchAllAssociative()[0];

            if (!$exists || isset($_POST['password']) && $password === '' || !password_verify($password, (string)$resultArray['password'])) {
                array_push($formErrors, 'ongeldige gebruikersnaam of paswoord');
            }

            if (empty($formErrors)) {
                $stmt = $connection->prepare('SELECT id, role FROM users WHERE username = ?;');
                $result = $stmt->executeQuery([$_POST['username']]);
                $resultArray = $result->fetchAllAssociative()[0];
                $_SESSION['username'] = $_POST['username'];
                $_SESSION['id'] = $resultArray['id'];
                $_SESSION['role'] = $resultArray['role'];

                header('Location: /');
                exit();
            }
        }

        $tpl = $this->twig->load('login.twig');
        echo $tpl->render([
            'formErrors' => $formErrors,
            'name' => $user
        ]);
    }
    public function register()
    {
        if (!empty($_SESSION['id'])){
            header('Location: /');
            exit();
        }
        $connection = $this->connection;
        // Initial Values
        $formerrors = [];

        $username = isset($_POST['username']) ? $_POST['username'] : ''; // The task that was sent via the form
        $name = isset($_POST['name']) ? $_POST['name'] : ''; // The priority that was sent via the form
        $lname = isset($_POST['lastname']) ? $_POST['lastname'] : '';
        $email = isset($_POST['mail']) ? $_POST['mail'] : ''; // The task that was sent via the form
        $number = isset($_POST['number']) ? $_POST['number'] : ''; // The priority that was sent via the form
        $password = isset($_POST['pass']) ? $_POST['pass'] : '';

        //formcheck
        if (isset($_POST['moduleAction']) && ($_POST['moduleAction'] === 'regis')) {

            $stmt = $connection->prepare('SELECT EXISTS(SELECT id FROM users WHERE username = ?);');
            $result = $stmt->executeQuery([$username]);
            $exists = $result->fetchAllAssociative();
            $exists = (boolean)$exists[0][array_key_first($exists[0])];
            if ($exists) {
                array_push($formerrors, 'Username is reeds in gebruik');
            }

            $stmt = $connection->prepare('SELECT EXISTS(SELECT id FROM users WHERE email = ?);');
            $result = $stmt->executeQuery([$email]);
            $exists = $result->fetchAllAssociative();
            $exists = (boolean)$exists[0][array_key_first($exists[0])];
            if ($exists) {
                array_push($formerrors, 'Email is reeds in gebruik');
            }
            //var_dump($username, $name, $lname, $email, $number, $password);
            if (isset($_POST['username']) && $username === '') {
                array_push($formerrors, 'Geef een geldige username in.');
            }
            if (isset($_POST['name']) && $name === '') {
                array_push($formerrors, 'Geef een geldige naam in.');
            }
            if (isset($_POST['lastname']) && $lname === '') {
                array_push($formerrors, 'Geef een geldige achternaam in.');
            }
            if (isset($_POST['mail']) && $email === '') {
                array_push($formerrors, 'Geef een geldige mailadress in.');
            }
            if (isset($_POST['number']) && $number === '' || strlen($number) !== 10) {
                array_push($formerrors, 'Geef een geldig telefoonnummer in.');
            }
            if (isset($_POST['pass']) && $password === '') {
                array_push($formerrors, 'Geef een paswoord in');
            }
            if (empty($formerrors)) {
                $nameForDb = $name . ' ' . $lname;
                var_dump($name);
                var_dump($password);
                $password = password_hash($password, PASSWORD_DEFAULT);
                var_dump($password);
                $result = $connection->prepare
                ('insert into users (username, email, name, phone_number, password, role, added_on)
                  values(:username, :email, :nameForDb, :phonenumber,:password, :role, NOW())');

                $result->bindValue(':username', $username);
                $result->bindValue(':email', $email);
                $result->bindValue(':nameForDb', $nameForDb);
                $result->bindValue(':phonenumber', $number);
                $result->bindValue(':password', $password);
                $result->bindValue(':role', 'client');

                $result->executeQuery();

                header('Location: /');
                exit();
            }
        }

        $tpl = $this->twig->load('registration.twig');
        echo $tpl->render([
            'formErrors' => $formerrors,
            'user' => $username,
            'fname' => $name,
            'lname' => $lname,
            'mail' => $email,
            'number' => $number
        ]);
    }
    public function logout(){
        if (empty($_SESSION['id'])){
            header('Location: /');
            exit();
        }

        $_SESSION = [];
        session_destroy();

        header('Location: /');
        exit();
    }
    public function getAgent($formErrors = [], $formValues = []){
        $id2 = 0;
        if (!empty($_SESSION['id'])){
            $id2 = $_SESSION['id'];
        }
        if ($this->currentUser['role'] !== 'admin'){
            header('Location: /');
            exit();
        }

        $query = 'SELECT name FROM estate_agencies';
        $query = $this->connection->query($query);
        $agencies = $query->fetchAllAssociative();

        var_dump($agencies);

        $tpl = $this->twig->load('addAgent.twig');
        echo $tpl->render([
            'user' => $this->currentUser,
            'id' => $id2,
            'formErrors' => $formErrors,
            'formValues' => $formValues,
            'agencies' => $agencies
        ]);
    }
    public function getAccounts(){

        $query = 'SELECT *, estate_agencies.id as agency_id, estate_agencies.name as agency_name, estate_agencies.images as agency_image 
                    , users.id as id, users.name as name, users.images as images
                    FROM users inner join estate_agencies on estate_agencies.id = users.estate_agencies_id where role = "agent"';
        $query = $this->connection->query($query);
        $agents = $query->fetchAllAssociative();

        $tpl = $this->twig->load('agents.twig');
        echo $tpl->render([
            'user' => $this->currentUser,
            'agents' => $agents
        ]);
    }
    public function agentCreate(){
        $formErrors = [];
        $name = isset($_POST['name']) ? $_POST['name'] : '';
        $mail = isset($_POST['mail']) ? $_POST['mail'] : '';
        $username = isset($_POST['username']) ? $_POST['username'] : '';
        $title = isset($_POST['title']) ? $_POST['title'] : '';
        $password = isset($_POST['password']) ? $_POST['password'] : '';
        $phone = isset($_POST['phone']) ? $_POST['phone'] : '';
        $formValues = [$name, $mail, $phone, $username, $title];

        $stmt = $this->connection->prepare("SELECT EXISTS(SELECT * from users where username = ?);");
        $result = $stmt->executeQuery([$username]);
        $exists = $result->fetchAllAssociative();
        $exists = (boolean)$exists[0][array_key_first($exists[0])];
        if($exists){
            array_push($formErrors, 'Username already in use');
        }
        $stmt = $this->connection->prepare("SELECT EXISTS(SELECT * from estate_agencies where name = ?);");
        $result = $stmt->executeQuery([$title]);
        $exists = $result->fetchAllAssociative();
        $exists = (boolean)$exists[0][array_key_first($exists[0])];
        if(!$exists){
            array_push($formErrors, 'Agency does not exist yet');
        }
        $stmt = $this->connection->prepare("SELECT EXISTS(SELECT * from users where name = ?);");
        $result = $stmt->executeQuery([$name]);
        $exists = $result->fetchAllAssociative();
        $exists = (boolean)$exists[0][array_key_first($exists[0])];
        if($exists){
            array_push($formErrors, 'Name already in use');
        }
        $stmt = $this->connection->prepare("SELECT EXISTS(SELECT * from users where email = ?);");
        $result = $stmt->executeQuery([$mail]);
        $exists = $result->fetchAllAssociative();
        $exists = (boolean)$exists[0][array_key_first($exists[0])];
        if($exists){
            array_push($formErrors, 'Email already in use');
        }

        if ($username == '' || strlen($username) < 6){
            array_push($formErrors, 'Give in a valid username');
        }
        if ($name == '' || strlen($name) < 6){
            array_push($formErrors, 'Give in a valid name');
        }
        if ($mail == '' || !str_contains($mail, '@') || !str_contains($mail, '.')){
            array_push($formErrors, 'Give in a valid email address');
        }
        if (!is_numeric($phone)){
            array_push($formErrors, 'Give in a valid phone number (ex. 0412345678)');
            var_dump($phone);
        }

        if (isset($_FILES['images']) && $_FILES['images']['name'][0] != '') {
            $allowedFormats = ['jpeg', 'jpg', 'png'];

            $extension = (new \SplFileInfo($_FILES['images']['name'][0]))->getExtension();
            if (in_array($extension, $allowedFormats)) {
                $moved = @move_uploaded_file($_FILES['images']['tmp_name'][0], './uploadImg/' . $_FILES['images']['name'][0]);
                if (!$moved) {
                    $formErrors[] = 'Error while saving image in the uploads folder';
                }
            } else {
                $msgImages = 'Only images of the type ';
                if (in_array($extension, $allowedFormats)) {
                    foreach ($allowedFormats as $i => $format) {
                        $msgImages .= $format;
                        $next = isset($allowedFormats[$i + 1]);
                        if ($next && $i != count($allowedFormats)) {
                            $msgImages .= ', ';
                        } else if ($next) {
                            $msgImages .= ' or ';
                        }
                    }
                }
                $msgImages .= ' are allowed';
                $formErrors[] = $msgImages;
            }
        }else{
            array_push($formErrors, 'Upload a picture');
        }
        if (empty($formErrors)){
            $stmtGetIdAgency = $this->connection->query('select id from estate_agencies where name = "'.$title.'"');
            $stmtGetIdAgency = $stmtGetIdAgency->fetchAssociative();

            $stmt = $this->connection->prepare('INSERT INTO users
                (username, images, email, name, phone_number, password, role, estate_agencies_id) VALUES (?,?,?,?,?,?,?,?);');
            $result = $stmt->executeStatement([$username, $_FILES['images']['name'][0], $mail, $name, $phone,
                password_hash($password, PASSWORD_DEFAULT), 'agent', $stmtGetIdAgency['id']]);

            header('Location: /');
            exit();
        }else{
            echo $this->getAgent($formErrors, $formValues);
        }
    }
}

?>