<?php
namespace Http;

class PropertiesController extends BaseController {

   private int $residencesPerPage = 5;
   protected array $cities;
   protected array $residenceTypes;
   private array $filters;
   

   public function __construct() {
      parent::__construct();

      $this->cities = $this->getCities();
      $this->residenceTypes = ['Apartment','Cottage','Penthouse','Mansion','Single-family','Multi-family','Bungalow','Townhouse','Ground'];
      $this->filters = ['All', 'New to Old', 'For Rent', 'For Sale'];
   }

   public function index(){
      // Action view single property
      if (isset($_POST['moduleAction']) && $_POST['moduleAction'] == 'single-residence') {
         header('Location: property-single.html');
         exit();
      }

      // get all residences/parcels from the database
      $residences = $this->getResidences(null, 9);
      $agents = $this->getAgents(3);

      $tpl = $this->twig->load('index.twig');
      echo $tpl->render([
         'residences' => array_slice($residences, 0, 3),
         'latest_properties' => $residences,
         'agents' => $agents,
         'user' => $this->currentUser,
         'types' => $this->residenceTypes,
         'cities' => $this->cities
      ]);
   }

   public function overview(int $currentPage) {
      $numberOfPages = $this->getNumberOfPages();
      // Action view single property
      if (isset($_POST['moduleAction']) && $_POST['moduleAction'] == 'single-residence') {
         header('Location: property-single.html');
         exit();
      }

      // get all residences/parcels from the database for one page
      $residences = $this->getResidences(null, $this->residencesPerPage, ($currentPage - 1) * $this->residencesPerPage);

      echo $this->twig->render('property-grid.twig', [
         'residences' => $residences,
         'pages' => $numberOfPages,
         'currentPage' => $currentPage,
         'user' => $this->currentUser,
         'types' => $this->residenceTypes,
         'cities' => $this->cities,
         'filters' => $this->filters,
         'currentFilter' => isset($parameters['filter']) ? $parameters['filter'] : 'All'
      ]);
   }

   public function view($id){
      $residencesQuery = 'SELECT *, residences.id AS residence_id FROM residences INNER JOIN properties ON properties_id = properties.id INNER JOIN addresses ON addresses_id = addresses.id WHERE residences.id = '. $id;
      $result = $this->connection->executeQuery($residencesQuery);
      $residence = $result->fetchAllAssociative()[0];
      
      $residence['images'] = explode(',',$residence['images']);

      echo $this->twig->render('property.twig', [
         'residence' => $residence,
         'user' => $this->currentUser,
         'types' => $this->residenceTypes,
         'cities' => $this->cities
      ]);
   }

   public function search($currentPage, $query) {
      $query = strstr($_SERVER['REQUEST_URI'], '?');
      $query = str_replace('?', '', $query);

      $parameters = [];

      parse_str($query, $parameters);
      
      $residences = $this->getResidences($parameters, $this->residencesPerPage, ($currentPage - 1) * $this->residencesPerPage);
      $pages = $this->getNumberOfPages($parameters);

      echo $this->twig->render('property-grid.twig', [
         'residences' => $residences,
         'pages' => $pages,
         'currentPage' => $currentPage, 
         'searchParams' => '?' . $query,
         'user' => $this->currentUser,
         'types' => $this->residenceTypes,
         'cities' => $this->cities,
         'filters' => $this->filters,
         'currentFilter' => isset($parameters['filter']) ? $parameters['filter'] : 'All'
      ]);
   }

   public function favourites(int $currentPage) {
      if (!$this->currentUser) {
         header('Location: /login');
         exit();
      }
      
      $residences = [];
      $favourites = $this->getUserFavourites();

      foreach ($favourites as $favourite) {
         $residences[] = $this->getResidence($favourite);
      }

      $pages = ceil(count($residences) / $this->residencesPerPage);

      echo $this->twig->render('property-grid.twig', [
         'residences' => $residences,
         'pages' => $pages > 0 ? $pages : 1,
         'currentPage' => $currentPage, 
         'user' => $this->currentUser,
         'types' => $this->residenceTypes,
         'cities' => $this->cities,
         'filters' => $this->filters,
         'currentFilter' => isset($parameters['filter']) ? $parameters['filter'] : 'All'
      ]);
   }

   public function savepreference($query) {
      if (!$this->currentUser) {
         header('Location: /login');
         exit();
      }
      $parameters = $this->getParametersUri($_SERVER['REQUEST_URI']);

      $result = $this->connection->executeStatement('
         INSERT INTO user_preferences
         (users_id, preferences)
         VALUES (?, ?)
      ', [$this->currentUser['id'], json_encode($parameters)]);

      if ($result) {
         header('Location: /residences/search/page/1?' . http_build_query($parameters));
         exit();
      }
      die('Server error'); // ----------------------------------------
   }

   public function deletepreference(int $preferenceId) {
      if (!$this->currentUser) {
         header('Location: /login');
         exit();
      }
      $preferences = $this->getUserPreferences($this->currentUser['id']);
      
      foreach ($preferences as $key => $preference) {
         if ($preferenceId == $key) {
            $deletePreference = $this->connection->executeStatement('DELETE FROM user_preferences WHERE id = ?', [$preferenceId]);
            if ($deletePreference) {
               header('Location: /residences/preferences');
               exit();
            }
            break;
         }
      }
   }

   public function viewPreferences() {
      if (!$this->currentUser) {
         header('Location: /login');
         exit();
      }
      $preferences = $this->getUserPreferences($this->currentUser['id']);


      echo $this->twig->render('preferences.twig', [
         'preferences' => $preferences,
         'user' => $this->currentUser,
         'types' => $this->residenceTypes,
         'cities' => $this->cities
      ]);
   }

   public function add(){
      if (!$this->currentUser) {
         header('Location: /login');
         exit();
      }
      $connection = $this->connection;

      $types = ["Apartment", "Cottage", "Penthouse", "Mansion", "Single-family", "Multi-family", "Bungalow", "Townhouse", "Ground"];
      $formErrors = [];

      $type = isset($_POST['type']) ? $_POST['type'] : ''; 
      $price = isset($_POST['price']) ? $_POST['price'] : '';
      $name = isset($_POST['name']) ? $_POST['name'] : ''; 
      $description = isset($_POST['description']) ? $_POST['description'] : ''; 
      $properties = isset($_POST['properties']) ? $_POST['properties'] : '';
      $address = isset($_POST['address']) ? $_POST['address'] : '';  

      if($properties == ''){
         $query = 'SHOW COLUMNS FROM properties';
         $result = $this->connection->executeQuery($query);
         $columns = $result->fetchAllAssociative();
         $properties = [];
   
         foreach($columns as $column){
            if($column['Field'] == 'id') continue;
            $properties[$column['Field']] = '';
         }
      }

      // Handle action 'add' (user pressed add button)
      if (isset($_POST['moduleAction']) && ($_POST['moduleAction'] === 'processUpload')) {

         $allOk = true;
         // check parameters
         // @TODO (if an error was encountered, add it to the $formErrors array)

         if(!in_array($type, $types)){
            $formErrors[] = 'No valid type selected';
            $allOk = false;
         }

         if(!is_numeric($price) && substr($price, 0, 1) != "-"){
            $formErrors[] = 'No valid price';
            $allOk = false;
         }

         if (trim($address['country']) === '') {
            $formErrors[] = 'Fill in a country';
            $allOk = false;
         }

         if (trim($address['city']) === '') {
            $formErrors[] = 'Fill in a city';
            $allOk = false;
         }

         if (!is_numeric($address['zip'])) {
            $formErrors[] = 'No valid zip code';
            $allOk = false;
         }

         if (trim($name) === '') {
            $formErrors[] = 'Fill in a name';
            $allOk = false;
         }

         if (trim($description) === '') {
            $formErrors[] = 'Fill in a description';
            $allOk = false;
         }

         foreach($properties as $key => $property){
            if(!is_numeric($property)){
               $formErrors[] = 'No valid ' . $key;
               $allOk = false;
            }
         }

         if (isset($_FILES['images']) && $_FILES['images']['name'][0] != '' && $allOk) {
            $formErrors[] = $this->checkImages($_FILES['images']);

         } else if($allOk){
            $allOk = false;
            $formErrors[] = 'Select at least one picture';
         }

         // @TODO if no errors: insert values into database
         // @TODO if insert query succeeded: redirect to this very same page
         if(empty($formErrors[0])){
            //---make properties-----------------------------------------------------------
            $query = 'SHOW COLUMNS FROM properties';
            $result = $this->connection->executeQuery($query);
            $unformattedColumns = $result->fetchAllAssociative();

            $valuesString = '';
            $columns = [];
            foreach($unformattedColumns as $count => $column){
               if($column['Field'] == 'id') {
                  continue;
               }
               if($count != 1) $valuesString .= ',';
               $valuesString .= '?';
               $columns[] = $column['Field'];
            }
            $stmt = $connection->prepare('INSERT INTO properties(' . implode(', ', $columns) . ') VALUES (' . $valuesString . ');');
            $result = $stmt->executeStatement(array_values($properties));
            $propertiesId = $connection->lastInsertId();

            //---make address-----------------------------------------------------------
            $stmt = $connection->prepare('INSERT INTO addresses(country, city, zip_code, street, street_number, street_bus) VALUES (?,?,?,?,?,?);');
            $result = $stmt->executeStatement(array_values($address));
            $addressId = $connection->lastInsertId();

            //---make residence with references----------------------------------------------
            $stmt = $connection->prepare('INSERT INTO residences(type, price, description, images, properties_id, addresses_id, estate_agencies_id, date_added) VALUES (?,?,?,?,?,?,?,NOW());');
            $result = $stmt->executeStatement([$type, $price, $description, implode(', ', $_FILES['images']['name']), $propertiesId, $addressId, $this->currentUser['estate_agencies_id']]);
            
            $residenceId = $connection->lastInsertId();

            $formErrors[] = $this->uploadImages($_FILES['images'], $this->basePath . '/public/img/residences/' . $residenceId);

            $users = $this->getUsers();
            foreach ($users as $user) {
               if ($this->isPreferenced($residenceId, $user['id'])) {
                  $this->sendMailNewPreference($residenceId, $user);
               }               
            }
            header('Location: /');
            exit();
         }
      }

      $tpl = $this->twig->load('addResidence.twig');
      echo $tpl->render([
         'user' => $this->currentUser, 
         'formErrors' => $formErrors,
         'types' => $types,
         'values' => $_POST,
         'properties' => $properties,
         'types' => $this->residenceTypes,
         'cities' => $this->cities
      ]);
   }

   public function edit($id){
      if (!$this->currentUser) {
         header('Location: /login');
         exit();
      }
      $connection = $this->connection;

      $stmtGetResidenceValues = $connection->query('
         select residences.type, residences.price, residences.description, residences.properties_id, residences.addresses_id, 
         addresses.country, addresses.city, addresses.street, addresses.street_number, addresses.zip_code, addresses.street_bus,
         properties.bedrooms, properties.bathrooms, properties.garages
         from residences 
         inner join properties on properties.id = residences.properties_id
         inner join addresses on addresses.id = residences.addresses_id
         where residences.id = "'.$id.'"');
      $stmtGetResidenceValues = $stmtGetResidenceValues->fetchAssociative();
      var_dump($stmtGetResidenceValues);

      $types = ["Apartment", "Cottage", "Penthouse", "Mansion", "Single-family", "Multi-family", "Bungalow", "Townhouse", "Ground"];
      $formErrors = [];

      $type = isset($_POST['type']) ? $_POST['type'] : '';
      $price = isset($_POST['price']) ? $_POST['price'] : '';
      $name = isset($_POST['name']) ? $_POST['name'] : '';
      $description = isset($_POST['description']) ? $_POST['description'] : '';
      $properties = isset($_POST['properties']) ? $_POST['properties'] : '';
      $address = isset($_POST['address']) ? $_POST['address'] : '';

      if($properties == ''){
         $query = 'SHOW COLUMNS FROM properties';
         $result = $this->connection->executeQuery($query);
         $columns = $result->fetchAllAssociative();
         $properties = [];

         foreach($columns as $column){
               if($column['Field'] == 'id') continue;
               $properties[$column['Field']] = '';
         }
      }

      // Handle action 'add' (user pressed add button)
      if (isset($_POST['moduleAction']) && ($_POST['moduleAction'] === 'processUpload')) {

         $allOk = true;
         // check parameters
         // @TODO (if an error was encountered, add it to the $formErrors array)

         if(!in_array($type, $types)){
               $formErrors[] = 'No valid type selected';
               $allOk = false;
         }

         if(!is_numeric($price) && substr($price, 0, 1) != "-"){
               $formErrors[] = 'No valid price';
               $allOk = false;
         }

         if (trim($address['country']) === '') {
               $formErrors[] = 'Fill in a country';
               $allOk = false;
         }

         if (trim($address['city']) === '') {
               $formErrors[] = 'Fill in a city';
               $allOk = false;
         }

         if (!is_numeric($address['zip'])) {
               $formErrors[] = 'No valid zip code';
               $allOk = false;
         }

         if (trim($name) === '') {
               $formErrors[] = 'Fill in a name';
               $allOk = false;
         }

         if (trim($description) === '') {
               $formErrors[] = 'Fill in a description';
               $allOk = false;
         }

         foreach($properties as $key => $property){
               if(!is_numeric($property)){
                  $formErrors[] = 'No valid ' . $key;
                  $allOk = false;
               }
         }

         if (isset($_FILES['images']) && $_FILES['images']['name'][0] != '' && $allOk) {
               $formErrors[] = $this->checkImages($_FILES['images']);

         } else if($allOk){
               $allOk = false;
               $formErrors[] = 'Select at least one picture';
         }

         // @TODO if no errors: insert values into database
         // @TODO if insert query succeeded: redirect to this very same page
         if(empty($formErrors[0])){
               //---make properties-----------------------------------------------------------
               $query = 'SHOW COLUMNS FROM properties';
               $result = $this->connection->executeQuery($query);
               $unformattedColumns = $result->fetchAllAssociative();

               $valuesString = '';
               $columns = [];

               foreach($unformattedColumns as $count => $column){
                  if($column['Field'] == 'id') {
                     continue;
                  }
                  if($count != 1) $valuesString .= ',';
                  $valuesString .= '?';
                  $columns[] = $column['Field'];
               }
               $values = implode(', ', $columns);
               $stmt = $connection->prepare('update properties set bedrooms = ?, bathrooms = ?, garages = ? where id = "'.$stmtGetResidenceValues['properties_id'].'";');
               $result = $stmt->executeStatement(array_values($properties));
               $propertiesId = $connection->lastInsertId();

               //---make address-----------------------------------------------------------
               $stmt = $connection->prepare('update addresses set country = ?, city = ?, zip_code =?, street=?, street_number=?, street_bus=? where id = "'.$stmtGetResidenceValues['addresses_id'].'";');
               $result = $stmt->executeStatement(array_values($address));
               $addressId = $connection->lastInsertId();

               //---make residence with references----------------------------------------------
               $stmt = $connection->prepare('update residences set type = ?, price = ?, description = ?, images = ?, properties_id = ?, addresses_id = ?, estate_agencies_id = ? where id = "'.$id.'";');
               $result = $stmt->executeStatement([$type, $price, $description, implode(', ', $_FILES['images']['name']), $stmtGetResidenceValues['properties_id'], $stmtGetResidenceValues['addresses_id'], $this->currentUser['estate_agencies_id']]);

               $residenceId = $connection->lastInsertId();

               $formErrors[] = $this->uploadImages($_FILES['images'], $this->basePath . '/public/img/residences/' . $residenceId);


               header('Location: /');
               exit();
         }
      }

      $tpl = $this->twig->load('addResidence.twig');
      echo $tpl->render([
         'user' => $this->currentUser,
         'formErrors' => $formErrors,
         'values' => $_POST,
         'properties' => $properties,
         'types' => $this->residenceTypes,
         'residenceValues' => $stmtGetResidenceValues,
         'cities' => $this->cities,
         'check' => true,
         'id' => $id
      ]);
   }

   public function like(int $id) {
      if (!$this->currentUser) {
         header('Location: /login');
         exit();
      }
      if (!$this->getResidence($id)) {
         die('server error'); // ---------------------------------------------
      }

      $likeResidenceQuery = $this->connection->prepare('
         INSERT INTO user_favourites (users_id, residences_id) VALUES (:user, :residence)
      ');
      $likeResidenceQuery->bindValue(':user', $_SESSION['id']);
      $likeResidenceQuery->bindValue(':residence', $id);

      $result = $likeResidenceQuery->executeStatement();
      if ($result) {
         $currentPage = (int)isset($_POST['currentPage']) ? ($_POST['currentPage'] > 0 ? $_POST['currentPage'] : 1) : 1;
         header('Location: /residences/page/' . $currentPage);
         exit();
      }      
   }

   public function unlike(int $id) {
      if (!$this->currentUser) {
         header('Location: /login');
         exit();
      }
      if (!$this->getResidence($id)) {
         die('server error'); // ---------------------------------------------
      }

      $likeResidenceQuery = $this->connection->prepare('
         DELETE FROM user_favourites WHERE residences_id = :residence
      ');
      $likeResidenceQuery->bindValue(':residence', $id);

      $result = $likeResidenceQuery->executeStatement();
      if ($result) {
         $currentPage = (int)isset($_POST['currentPage']) ? ($_POST['currentPage'] > 0 ? $_POST['currentPage'] : 1) : 1;
         header('Location: /residences/page/' . $currentPage);
         exit();
      } else {
         die('server error'); // ---------------------------------------------
      }
   }

   private function getNumberOfPages(?array $searchParams = null) : int {
      $residencesQuery = '
         SELECT COUNT(residences.id) FROM residences
         INNER JOIN properties ON properties.id = residences.properties_id
         INNER JOIN addresses ON addresses.id = residences.addresses_id
         INNER JOIN estate_agencies ON estate_agencies.id = residences.estate_agencies_id
      ';

      if (isset($searchParams)) {
         $extraParams = $this->addQueryParams($searchParams);
         $residencesQuery .= $extraParams['query'];
      }

      $residencesQuery = $this->connection->prepare($residencesQuery);

      if (isset($searchParams)) {
         foreach ($extraParams['params'] as $key => $value) {
            $residencesQuery->bindValue($key, $value);
         }
      }

      $residencesCount = $residencesQuery->executeQuery()->fetchOne();
      $pages = ceil($residencesCount / $this->residencesPerPage);

      return $pages > 0 ? $pages : 1;
   }

   private function getParametersUri(string $uri) : array {
      $query = strstr($uri, '?');
      $query = str_replace('?', '', $query);

      $parameters = [];
      parse_str($query, $parameters);

      return $parameters;
   }

   private function getResidence(int $id) {
      if (!is_numeric($id)) {
         return null;
      }
      $residenceQuery = $this->connection->prepare('
         SELECT residences.id, type, description, price, payment, surface, residences.images, 
            energy_label, bedrooms, bathrooms, garages, addresses.country, addresses.city, addresses.zip_code, addresses.street,
            addresses.street_number, addresses.street_bus, estate_agencies.name, estate_agencies.id AS agency_id
         FROM residences
         INNER JOIN properties ON properties.id = residences.properties_id
         INNER JOIN addresses ON addresses.id = residences.addresses_id
         INNER JOIN estate_agencies ON estate_agencies.id = residences.estate_agencies_id
         WHERE residences.id = :id 
      ');
      $residenceQuery->bindValue(':id', $id);
      $result = $residenceQuery->executeQuery();

      if ($result) {
         $residence = $result->fetchAssociative();
         $residence['images'] = explode(',', str_replace(' ', '', $residence['images']));
         $residence['favourite'] = $this->isUserFavourite($residence['id']);
         return $residence;
      } 
      return null;
   }

   private function getUserPreferences($id) {
      $preferences = [];
      $preferencesDB = $this->connection->executeQuery('
         SELECT id, preferences FROM user_preferences
         WHERE users_id = ?
      ', [$id]);

      if ($preferencesDB) { 
         $preferencesDB = $preferencesDB->fetchAllAssociative();
         foreach ($preferencesDB as $preference) {
            $preferences[$preference['id']] = (array)json_decode($preference['preferences']);
            
            $preferences[$preference['id']]['min-price'] = number_format($preferences[$preference['id']]['min-price']);
            $preferences[$preference['id']]['max-price'] = number_format($preferences[$preference['id']]['max-price']);
         }
         return $preferences;
      }
      return null;
   }

   private function isPreferenced(int $residenceId, int $userId) {
      $residence = $this->getResidence($residenceId);
      $preferences = $this->getUserPreferences($userId);
      $residencesWithPreferences = [];

      foreach ($preferences as $preference) {
         $residencesWithPreferences[] = $this->getResidences($preference);
      }
      $residencesWithPreferences = array_merge(...$residencesWithPreferences);

      foreach ($residencesWithPreferences as $residenceWithPreference) {
         if ($residenceWithPreference['id'] === $residence['id']) {
            return true;
         }
      }
      return false;
   }

   private function sendMailNewPreference(int $residenceId, array $user) {
      $residence = $this->getResidence($residenceId);

      $body = $this->twig->render('mailNewPreference.twig', [
         'residence' => $residence
      ]);

      $this->mailer->send($user, $body, 'A new residence has been uploaded that you might be interested in');
   }

   private function checkImages(array $images) {
      foreach ($images['error'] as $image) {
         if ($image === 1) {
            return 'Max file size is 5MB';
         }
      }

      if (!$this->checkFileSize($images['size'])) {
         return 'Max file size is 5MB';
      }

      if (!$this->checkExtension($images['type'])) {
         return 'Allowed file types are jpg, jpeg';
      }

      if (count($images) > 50) {
         return 'Max number of images is 50';
      }
   }

   private function uploadImages(array $images, string $path) {
      if (!file_exists($path)) {
         if (!mkdir($path)) {
            return 'Server error: unable to create directory';
         }
      }

      for ($i=0; $i < count($images['name']); $i++) { 
         if (!move_uploaded_file($images['tmp_name'][$i], $path . '/' . $images['name'][$i]) || $images['error'][$i] !== 0) {
            return 'An error has occurred while uploading the images';
         }
      }
   }

   private function checkExtension(array $extensions) {
      foreach ($extensions as $extension) {
         echo $extension . PHP_EOL;
         if (!str_contains($extension, 'image')) {
            return false;
         }
      }
      return true;
   }

   private function checkFileSize(array $sizes) {
      $totalSize = 0;

      foreach ($sizes as $size) {
         $totalSize += $size;
         if ($size > 5_000_000) {
            return false;
         }
      }
      return $totalSize < 256_000_000;
   }
}
?>