<?php
namespace Http;

class BaseController{

   protected $connection;
   protected $twig;
   protected $mailer;
   protected $basePath;

   protected array $currentUser;

   public function __construct() {
      $this->basePath = dirname(dirname(__DIR__));

      $this->connection = \Services\DatabaseConnector::getConnection();

      $this->mailer = new \Services\Mailer();

      $loader = new \Twig\Loader\FilesystemLoader($this->basePath . '/resources/templates');
      $this->twig = new \Twig\Environment($loader, [
         'cache' => __DIR__ . '/../../storage/cache',
         'auto_reload' => true // set to false on production
      ]);

      

      $this->currentUser = isset($_SESSION['id']) ? $this->getUser($_SESSION['id']) : [];
   }

   protected function getResidences(?array $searchParams = null, ?int $limit = null, ?int $offset = null) {
      $residences = [];
      $queryParams = [];
      $extraParams = [];

      $residencesQuery = '
         SELECT residences.id, type, description, price, payment, surface, residences.images, 
            energy_label, bedrooms, bathrooms, garages, addresses.country, addresses.city, addresses.zip_code, addresses.street,
            addresses.street_number, addresses.street_bus, estate_agencies.name, estate_agencies.id AS agency_id
         FROM residences
         LEFT JOIN properties ON properties.id = residences.properties_id
         LEFT JOIN addresses ON addresses.id = residences.addresses_id
         LEFT JOIN estate_agencies ON estate_agencies.id = residences.estate_agencies_id 
      ';

      if (isset($searchParams)) {
         $extraParams = $this->addQueryParams($searchParams);
         $residencesQuery .= $extraParams['query'];
      } else {
         $residencesQuery .= ' ORDER BY residences.id';
      }

      if (isset($limit)) {
         $residencesQuery .= ' LIMIT :limit';
         $queryParams[':limit'] = $limit;
      }

      if (isset($offset)) {
         $residencesQuery .= ' OFFSET :offset';
         $queryParams[':offset'] = $offset;
      }
      
      $residencesQuery = $this->connection->prepare($residencesQuery);
     
      foreach ($queryParams as $key => $value) {
         $residencesQuery->bindValue($key, $value, \Doctrine\DBAL\ParameterType::INTEGER);
      }

      if ($searchParams) {
         foreach ($extraParams['params'] as $key => $value) {
            $residencesQuery->bindValue($key, $value);
         }
      }

      
      $result = $residencesQuery->executeQuery();

      if ($result) {
         $residencesDB = $result->fetchAllAssociative();

         foreach ($residencesDB as $residence) {
            $residence['images'] = explode(',', str_replace(' ', '', $residence['images']));
            $residence['price'] = number_format($residence['price']);
            $residence['favourite'] = $this->isUserFavourite($residence['id']);
            $residences[] = $residence;
         }
      }
      return $residences;
   }

   protected function getAgents(?int $limit = null) {
      $agents = [];
      $agentsQuery = '
         SELECT estate_agencies.id AS agency_id, estate_agencies.name AS agency_name, users.id AS user_id, username, email, users.name AS user_name, users.images as images, phone_number, password FROM `estate_agencies`
         INNER JOIN users ON users.estate_agencies_id = estate_agencies.id
      ';

      if ($limit) {
         $agentsQuery .= ' LIMIT '.$limit;
      }

      $result = $this->connection->executeQuery($agentsQuery);
      if ($result) {
         $agents = $result->fetchAllAssociative();
      }

      return $agents;
   }

   protected function getAgentByAgency(int $id) {
      $agentQuery = $this->connection->prepare('
         SELECT estate_agencies.id AS agency_id, estate_agencies.name AS agency_name, users.id AS user_id, username, email, users.name AS user_name, phone_number, password FROM `estate_agencies`
         INNER JOIN users ON users.estate_agencies_id = estate_agencies.id
         WHERE estate_agencies.id = :id
      ');
      $agentQuery->bindValue(':id', $id);
      $result = $agentQuery->executeQuery();

      if ($result) {
         return $result->fetchAssociative();
      }
   }

   protected function getCities() {
      $citiesDB = $this->connection->executeQuery('SELECT DISTINCT city FROM addresses')->fetchAllAssociative();
      $cities = [];
      foreach ($citiesDB as $value) {
         $cities[] = $value['city'];
      }
      return $cities;
   }



   protected function getUser(int $id) {
      if (!is_numeric($id)) {
         return null;
      }

      $user = $this->connection->executeQuery('SELECT * FROM `users` WHERE users.id = ?', [$id]);

      if ($user) {
         return $user->fetchAssociative();
      }
      return null;
   }

   protected function getUsers() {
      $usersDB = $this->connection->executeQuery('SELECT * FROM users')->fetchAllAssociative();
      $users = [];
      foreach ($usersDB as $user) {
         $users[] = $user;
      }
      return $users;
   }

   protected function addQueryParams(array $parameters) {
      $query = '';
      $queryParams = [];
      
      if (isset($parameters['keyword']) && trim($parameters['keyword']) !== '') {
         $query .= $this->addWhereOrAnd($query);
         $query .= ' residences.description LIKE :keyword1 OR estate_agencies.name LIKE :keyword2';
         $queryParams[':keyword1'] = '%' . $parameters['keyword'] . '%';
         $queryParams[':keyword2'] = '%' . $parameters['keyword'] . '%';
      }

      if (isset($parameters['type']) && in_array($parameters['type'], $this->residenceTypes)) {
         $query .= $this->addWhereOrAnd($query);
         $query .= ' type = :type';
         $queryParams[':type'] = $parameters['type'];
      }

      if (isset($parameters['city']) && in_array($parameters['city'], $this->cities)) {
         $query .= $this->addWhereOrAnd($query);
         $query .= ' city = :city';
         $queryParams[':city'] = $parameters['city'];
      }

      if (isset($parameters['bedrooms']) && is_numeric($parameters['bedrooms'])) {
         $query .= $this->addWhereOrAnd($query);
         $query .= ' bedrooms >= :bedrooms';
         $queryParams[':bedrooms'] = $parameters['bedrooms'];
      }

      if (isset($parameters['bathrooms']) && is_numeric($parameters['bathrooms'])) {
         $query .= $this->addWhereOrAnd($query);
         $query .= ' bathrooms >= :bathrooms';
         $queryParams[':bathrooms'] = $parameters['bathrooms'];
      }

      if (isset($parameters['garages']) && is_numeric($parameters['garages'])) {
         $query .= $this->addWhereOrAnd($query);
         $query .= ' garages >= :garages';
         $queryParams[':garages'] = $parameters['garages'];
      }

      if (isset($parameters['min-price']) && is_numeric($parameters['min-price'])) {
         $query .= $this->addWhereOrAnd($query);
         $query .= ' price >= :minPrice';
         $queryParams[':minPrice'] = $parameters['min-price'];
      }
      
      if (isset($parameters['max-price']) && is_numeric($parameters['max-price']) && $parameters['max-price'] > 0) {
         $query .= $this->addWhereOrAnd($query);
         $query .= ' price <= :maxPrice';
         $queryParams[':maxPrice'] = $parameters['max-price'];
      }

      if (isset($parameters['agency']) && is_numeric($parameters['agency'])) {
         $query .= $this->addWhereOrAnd($query);
         $query .= ' estate_agencies_id = :agency';
         $queryParams[':agency'] = $parameters['agency'];
      }

      
      if (isset($parameters['filter'])) {
         $filter = $parameters['filter'];

         if ($filter === 'For Rent') {
            $query .= $this->addWhereOrAnd($query);
            $query .= ' payment = "for rent"';

         } else if ($filter === 'For Sale') {
            $query .= $this->addWhereOrAnd($query);
            $query .= ' payment = "for sale"';

         } else if ($filter === 'New to Old') {
            $query .= ' ORDER BY residences.date_added DESC';

         } else {
            $query .= ' ORDER BY residences.id';
         }
      }
      

      return ['query' => $query, 'params' => $queryParams];
   }

   protected function addWhereOrAnd($query) {
      return str_contains($query, 'WHERE') ? ' AND' : ' WHERE';
   }

   protected function isUserFavourite(int $id) {
      $favourites = $this->getUserFavourites();
      if (empty($favourites)) {
         return false;
      }
      foreach ($favourites as $favourite) {
         if ($favourite == $id) {
            return true;  
         }
      }
      return false;      
   }

   protected function getUserFavourites() {
      $id = !empty($this->currentUser) ? $this->currentUser['id'] : null;
      $favouritesDB = $this->connection->executeQuery('
         SELECT residences.id FROM user_favourites
         INNER JOIN residences ON residences.id = user_favourites.residences_id
         WHERE users_id = ?', [$id]
      );
      if ($favouritesDB) {
         $favouritesDB = $favouritesDB->fetchAllAssociative();
         $favourites = [];
         foreach ($favouritesDB as $favourite) {
            $favourites[] = $favourite['id'];
         }
         return $favourites;
      }
      return null;
   }

}
?>