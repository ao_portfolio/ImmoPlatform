<?php

namespace Http;

use SplFileInfo;
class AgencyController extends BaseController
{
    public function account(){
        if (!$this->currentUser) {
            header('Location: /login');
            exit();
        }
        $agency = $this->isAgent($this->currentUser['id']);

        echo $this->twig->render('myAccount.twig', [
            'user' => $this->currentUser,
            'agency' => $agency
        ]);
    }

    public function agencyPage($id){
        $agent = $this->getAgentByAgency($id);
        if (!$agent) {
            header('Location: /');
            exit();
        }
        $agency = $this->isAgent($agent['user_id']);
        $residences = $this->getResidences(['agency' => $agent['agency_id']]);

        echo $this->twig->render('agency.twig', [
            'user' => $this->currentUser,
            'agent' => $agent,
            'agency' => $agency,
            'residences' => $residences
        ]);
    }

    public function getAgency($formErrors = [], $formValues = []){
        if ($this->currentUser['role'] !== 'admin'){
            header('Location: /');
            exit();
        }
        $tpl = $this->twig->load('addAgency.twig');
        echo $tpl->render([
            'user' => $this->currentUser,
            'formErrors' => $formErrors,
            'formValues' => $formValues,
            'exist' => false,
            'check' => false,
            'path' => $_SERVER['REQUEST_URI']
        ]);
    }
    public function getAgencyForEdit($id, $formErrors = [], $formValues = []){
        $stmtGetEstateInfo = $this->connection->query('select * from estate_agencies where id = "'.$id.'"');
        $stmtGetEstateInfo = $stmtGetEstateInfo->fetchAssociative();

        $stmt = $this->connection->prepare("SELECT EXISTS(SELECT * from estate_agencies where name = ?);");
        $result = $stmt->executeQuery([$stmtGetEstateInfo['name']]);
        $exists = $result->fetchAllAssociative();
        $exists = (boolean)$exists[0][array_key_first($exists[0])];

        if (!$exists || $this->currentUser['estate_agencies_id'] !== $id){
            header('Location: /');
            exit();
        }
        $tpl = $this->twig->load('addAgency.twig');
        echo $tpl->render([
            'user' => $this->currentUser,
            'formErrors' => $formErrors,
            'formValues' => $formValues,
            'id2' => $id,
            'exist' => true,
            'check' => true
        ]);
    }
    public function agencyCreate(){
        $connection = $this->connection;

        $formErrors = [];
        $name = isset($_POST['name']) ? $_POST['name'] : '';
        $country = isset($_POST['country']) ? $_POST['country'] : '';
        $city = isset($_POST['city']) ? $_POST['city'] : '';
        $zip = isset($_POST['zip']) ? $_POST['zip'] : '';
        $street = isset($_POST['street']) ? $_POST['street'] : '';
        $number = isset($_POST['number']) ? $_POST['number'] : '';
        $formValues = [$name, $country, $city, $zip, $street, $number];

        $stmt = $this->connection->prepare("SELECT EXISTS(SELECT * from users where name = ?);");
        $result = $stmt->executeQuery([$name]);
        $exists = $result->fetchAllAssociative();
        $exists = (boolean)$exists[0][array_key_first($exists[0])];
        if($exists){
            array_push($formErrors, 'account already exists');
        }

        if ($name == '' || strlen($name) < 6){
            array_push($formErrors, 'Give in a valid name');
        }
        if ($country == '' || strlen($country) < 4){
            array_push($formErrors, 'Give in a valid country name');
        }
        if ($city == ''){
            array_push($formErrors, 'Give in a valid city name');
        }
        if (!is_numeric($zip)){
            array_push($formErrors, 'Give in a valid postal code');
        }
        if ($street == ''){
            array_push($formErrors, 'Give in a valid street name');
        }
        if (!is_numeric($number)){
            array_push($formErrors, 'Give in a valid street number');
        }

        if (isset($_FILES['images']) && $_FILES['images']['name'][0] != '') {
            $allowedFormats = ['jpeg', 'jpg', 'png'];

            $extension = (new \SplFileInfo($_FILES['images']['name'][0]))->getExtension();
            if (in_array($extension, $allowedFormats)) {
                $moved = @move_uploaded_file($_FILES['images']['tmp_name'][0], './uploadImg/' . $_FILES['images']['name'][0]);
                if (!$moved) {
                    $formErrors[] = 'Error while saving image in the uploads folder';
                }
            } else {
                $msgImages = 'Only images of the type ';
                if (in_array($extension, $allowedFormats)) {
                    foreach ($allowedFormats as $i => $format) {
                        $msgImages .= $format;
                        $next = isset($allowedFormats[$i + 1]);
                        if ($next && $i != count($allowedFormats)) {
                            $msgImages .= ', ';
                        } else if ($next) {
                            $msgImages .= ' or ';
                        }
                    }
                }
                var_dump($_FILES['images']['error'] === UPLOAD_ERR_OK);
                $msgImages .= ' are allowed';
                $formErrors[] = $msgImages;
            }
        }
        if (empty($formErrors)){
            $stmt = $connection->prepare('INSERT INTO addresses (country, city, zip_code, street, street_number) Values
                (?,?,?,?,?);');
            $result = $stmt->executeStatement([$country, $city,$zip, $street, $number]);

            $stmtGetIdMail = $this->connection->query('select id from addresses where  
                                     city = "'.$city.'" and 
                                     street = "'.$street.'" and 
                                     street_number = "'.$number.'"');
            $stmtGetIdMail = $stmtGetIdMail->fetchAssociative();
            var_dump($stmtGetIdMail);
            $stmt = $connection->prepare('INSERT INTO estate_agencies
                (name, blocked, addresses_id, images) 
                VALUES (?, false, ?, ?);');
            $result = $stmt->executeStatement([$name, $stmtGetIdMail['id'], $_FILES['images']['name'][0]]);

            header('Location: /dashboard');
            exit();
        }else{
            echo $this->getAgency($formErrors, $formValues);
        }
    }
    public function editAgency($id){
        $connection = $this->connection;

        $stmtGetEstateInfo = $this->connection->query('select * from estate_agencies where id = "'.$id.'"');
        $stmtGetEstateInfo = $stmtGetEstateInfo->fetchAssociative();

        $formErrors = [];
        $name = isset($_POST['name']) ? $_POST['name'] : '';
        $country = isset($_POST['country']) ? $_POST['country'] : '';
        $city = isset($_POST['city']) ? $_POST['city'] : '';
        $zip = isset($_POST['zip']) ? $_POST['zip'] : '';
        $street = isset($_POST['street']) ? $_POST['street'] : '';
        $number = isset($_POST['number']) ? $_POST['number'] : '';
        $formValues = [$name, $country, $city, $zip, $street, $number];

        $stmt = $this->connection->prepare("SELECT EXISTS(SELECT * from estate_agencies where name = ?);");
        $result = $stmt->executeQuery([$name]);
        $exists = $result->fetchAllAssociative();
        $exists = (boolean)$exists[0][array_key_first($exists[0])];
        if($exists && ($stmtGetEstateInfo['name']) != $name){
            array_push($formErrors, 'Agency already exists');
        }

        if ($name == '' || strlen($name) < 6){
            array_push($formErrors, 'Give in a valid name');
        }
        if ($country == '' || strlen($country) < 4){
            array_push($formErrors, 'Give in a valid country name');
        }
        if ($city == ''){
            array_push($formErrors, 'Give in a valid city name');
        }
        if (!is_numeric($zip)){
            array_push($formErrors, 'Give in a valid postal code');
        }
        if ($street == ''){
            array_push($formErrors, 'Give in a valid street name');
        }
        if (!is_numeric($number)){
            array_push($formErrors, 'Give in a valid street number');
        }

        if (isset($_FILES['images']) && $_FILES['images']['name'][0] != '') {
            $allowedFormats = ['jpeg', 'jpg', 'png'];

            $extension = (new \SplFileInfo($_FILES['images']['name'][0]))->getExtension();
            if (in_array($extension, $allowedFormats)) {
                $moved = @move_uploaded_file($_FILES['images']['tmp_name'][0], './uploadImg/' . $_FILES['images']['name'][0]);
                if (!$moved) {
                    $formErrors[] = 'Error while saving image in the uploads folder';
                }
            } else {
                $msgImages = 'Only images of the type ';
                if (in_array($extension, $allowedFormats)) {
                    foreach ($allowedFormats as $i => $format) {
                        $msgImages .= $format;
                        $next = isset($allowedFormats[$i + 1]);
                        if ($next && $i != count($allowedFormats)) {
                            $msgImages .= ', ';
                        } else if ($next) {
                            $msgImages .= ' or ';
                        }
                    }
                }
                var_dump($_FILES['images']['error'] === UPLOAD_ERR_OK);
                $msgImages .= ' are allowed';
                $formErrors[] = $msgImages;
            }
        }
        if (empty($formErrors)){
            $stmtGetIdMail = $this->connection->query('select addresses_id from estate_agencies where id = "'.$id.'"');
            $stmtGetIdMail = $stmtGetIdMail->fetchAssociative();


            $stmt = $connection->prepare('update addresses set country = ?, city = ?, zip_code = ?, street=?, street_number=? where id = ?;');
            $result = $stmt->executeStatement([$country, $city,$zip, $street, $number, $stmtGetIdMail['addresses_id']]);

            var_dump($stmtGetIdMail['addresses_id']);
            $stmt = $connection->prepare('update estate_agencies set name = ?, blocked = false, addresses_id=?, images=? where id = ?;');
            $result = $stmt->executeStatement([$name, $stmtGetIdMail['addresses_id'], $_FILES['images']['name'][0], $id]);

            header('Location: /agency/'.$id);
            exit();
        }else{
            echo $this->getAgencyForEdit($id, $formErrors, $formValues);
        }
    }


    private function isAgent(int $userId) {
        $agency = $this->connection->executeQuery('
            SELECT estate_agencies.id, estate_agencies.name, estate_agencies.images,
                country, city, zip_code, street, street_number, street_bus 
            FROM `estate_agencies` 
            INNER JOIN addresses ON addresses.id = estate_agencies.addresses_id
            INNER JOIN users ON users.estate_agencies_id = estate_agencies.id
            WHERE users.id = ?
        ', [$userId])->fetchAssociative();

        return $agency;
    }
}