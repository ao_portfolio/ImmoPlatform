<?php
namespace Http;

class DashboardController extends BaseController {

   public function __construct() {
      parent::__construct();

      if(!isset($_SESSION['username']) && $_SERVER['role'] !== 'admin') {
         header('Location: /');
         exit();
      }
   }

   public function index(){
       if ($this->currentUser['role'] !== 'admin'){
           header('Location: /');
           exit();
       }

      $query = "SELECT Count(*) as agencyCount FROM estate_agencies WHERE blocked = 0";
      $stmt = $this->connection->prepare($query);
      $result = $stmt->executeQuery([]);
      $statistics = $result->fetchAllAssociative()[0];

      $query = "SELECT Count(*) as agentCount FROM users WHERE role = 'agent'";
      $stmt = $this->connection->prepare($query);
      $result = $stmt->executeQuery([]);
      $statistics += $result->fetchAllAssociative()[0];

      $query = "SELECT Count(*) as clientCount FROM users WHERE role = 'client'";
      $stmt = $this->connection->prepare($query);
      $result = $stmt->executeQuery([]);
      $statistics += $result->fetchAllAssociative()[0];

      $query = "SELECT Count(*) as residenceCount FROM residences";
      $stmt = $this->connection->prepare($query);
      $result = $stmt->executeQuery([]);
      $statistics += $result->fetchAllAssociative()[0];

      $query = "SELECT date_added FROM residences";
      $stmt = $this->connection->prepare($query);
      $result = $stmt->executeQuery([]);
      $graphdata = $result->fetchAllAssociative();

      $dates = ["day" => [], "week" => [], "month" => []];
      //prefill 0's
      $today = date('Y-m-d');
      $thisWeek = $this->getWeekStartDate();
      $thisMonth = strtotime(date('Y-m'));

      foreach($dates as $key => $type){
         $dates[$key]["today"] = $today;
         for($i = 0; $i < 7; $i++){
            $dates[$key][$i] = ["count" => 0];
         }
      }
      $today = strtotime($today);
      
      //fill data
      foreach($graphdata as $date){
         $date = date('Y-m-d', strtotime($date['date_added']));
      
         switch ($date) {
            case $today:
               if(isset($dates["day"][6]["count"])){
                  $dates["day"][6]["count"]++;
               }
               else{
                  $dates["day"][6]["count"] = 0;
                  $dates["day"][6]["date"] = $date;
               }
               break;
            case date('Y-m-d',(strtotime ( '-1 day', $today) )):
               if(isset($dates["day"][5]["count"])){
                  $dates["day"][5]["count"]++;
               }
               else{
                  $dates["day"][5]["count"] = 0;
                  $dates["day"][5]["date"] = $date;
               }
                break;
            case date('Y-m-d',(strtotime ( '-2 day' , $today ) )):
               if(isset($dates["day"][4]["count"])){
                  $dates["day"][4]["count"]++;
               }
               else{
                  $dates["day"][4]["count"] = 0;
                  $dates["day"][4]["date"] = $date;
               }
               break;
            case date('Y-m-d',(strtotime ( '-3 day' , $today ) )):
               if(isset($dates["day"][3]["count"])){
                  $dates["day"][3]["count"]++;
               }
               else{
                  $dates["day"][3]["count"] = 0;
                  $dates["day"][3]["date"] = $date;
               }
               break;
            case date('Y-m-d',(strtotime ( '-4 day' , $today ) )):
               if(isset($dates["day"][2]["count"])){
                  $dates["day"][2]["count"]++;
               }
               else{
                  $dates["day"][2]["count"] = 0;
                  $dates["day"][2]["date"] = $date;
               }
               break;
            case date('Y-m-d',(strtotime ( '-5 day' , $today ) )):
               if(isset($dates["day"][1]["count"])){
                  $dates["day"][1]["count"]++;
               }
               else{
                  $dates["day"][1]["count"] = 0;
                  $dates["day"][1]["date"] = $date;
               }
               break;
            case date('Y-m-d',(strtotime ( '-6 day' , $today ) )):
               if(isset($dates["day"][0]["count"])){
                  $dates["day"][0]["count"]++;
               }
               else{
                  $dates["day"][0]["count"] = 0;
                  $dates["day"][0]["date"] = $date;
               }
               break;
         }

         switch (true) {
            case $date >= date('Y-m-d',(strtotime ( '-0 week' , $thisWeek ) )):
               if(isset($dates["week"][6]["count"])){
                  $dates["week"][6]["count"]++;
               }
               else{
                  $dates["week"][6]["count"] = 0;
                  $dates["week"][6]["date"] = $date;
               }
               break;
            case $date >= date('Y-m-d',(strtotime ( '-1 week' , $thisWeek ) )):
               if(isset($dates["week"][5]["count"])){
                  $dates["week"][5]["count"]++;
               }
               else{
                  $dates["week"][5]["count"] = 0;
                  $dates["week"][5]["date"] = $date;
               }
               break;
            case $date >= date('Y-m-d',(strtotime ( '-2 week' , $thisWeek ) )):
               if(isset($dates["week"][4]["count"])){
                  $dates["week"][4]["count"]++;
               }
               else{
                  $dates["week"][4]["count"] = 0;
                  $dates["week"][4]["date"] = $date;
               }
               break;
            case $date >= date('Y-m-d',(strtotime ( '-3 week' , $thisWeek ) )):
               if(isset($dates["week"][3]["count"])){
                  $dates["week"][3]["count"]++;
               }
               else{
                  $dates["week"][3]["count"] = 0;
                  $dates["week"][3]["date"] = $date;
               }
               break;
            case $date >= date('Y-m-d',(strtotime ( '-4 week' , $thisWeek ) )):
               if(isset($dates["week"][2]["count"])){
                  $dates["week"][2]["count"]++;
               }
               else{
                  $dates["week"][2]["count"] = 0;
                  $dates["week"][2]["date"] = $date;
               }
               break;
            case $date >= date('Y-m-d',(strtotime ( '-5 week' , $thisWeek ) )):
               if(isset($dates["week"][1]["count"])){
                  $dates["week"][1]["count"]++;
               }
               else{
                  $dates["week"][1]["count"] = 0;
                  $dates["week"][1]["date"] = $date;
               }
               break;
            case $date >= date('Y-m-d',(strtotime ( '-6 week' , $thisWeek ) )):
               if(isset($dates["week"][0]["count"])){
                  $dates["week"][0]["count"]++;
               }
               else{
                  $dates["week"][0]["count"] = 0;
                  $dates["week"][0]["date"] = $date;
               }
               break;
         }
         switch (true) {
            case $date >= date('Y-m-d',(strtotime ( '-0 month' , $thisMonth ) )):
               if(isset($dates["month"][6]["count"])){
                  $dates["month"][6]["count"]++;
               }
               else{
                  $dates["month"][6]["count"] = 0;
                  $dates["month"][6]["date"] = $date;
               }
               break;
            case $date >= date('Y-m-d',(strtotime ( '-1 month' , $thisMonth ) )):
               if(isset($dates["month"][5]["count"])){
                  $dates["month"][5]["count"]++;
               }
               else{
                  $dates["month"][5]["count"] = 0;
                  $dates["month"][5]["date"] = $date;
               }
               break;
            case $date >= date('Y-m-d',(strtotime ( '-2 month' , $thisMonth ) )):
               if(isset($dates["month"][4]["count"])){
                  $dates["month"][4]["count"]++;
               }
               else{
                  $dates["month"][4]["count"] = 0;
                  $dates["month"][4]["date"] = $date;
               }
               break;
            case $date >= date('Y-m-d',(strtotime ( '-3 month' , $thisMonth ) )):
               if(isset($dates["month"][3]["count"])){
                  $dates["month"][3]["count"]++;
               }
               else{
                  $dates["month"][3]["count"] = 0;
                  $dates["month"][3]["date"] = $date;
               }
               break;
            case $date >= date('Y-m-d',(strtotime ( '-4 month' , $thisMonth ) )):
               if(isset($dates["month"][2]["count"])){
                  $dates["month"][2]["count"]++;
               }
               else{
                  $dates["month"][2]["count"] = 0;
                  $dates["month"][2]["date"] = $date;
               }
               break;
            case $date >= date('Y-m-d',(strtotime ( '-5 month' , $thisMonth ) )):
               if(isset($dates["month"][1]["count"])){
                  $dates["month"][1]["count"]++;
               }
               else{
                  $dates["month"][1]["count"] = 0;
                  $dates["month"][1]["date"] = $date;
               }
               break;
            case $date >= date('Y-m-d',(strtotime ( '-6 month' , $thisMonth ) )):
               if(isset($dates["month"][0]["count"])){
                  $dates["month"][0]["count"]++;
               }
               else{
                  $dates["month"][0]["count"] = 0;
                  $dates["month"][0]["date"] = $date;
               }
               break;
         }
         

      }

      $tpl = $this->twig->load('dashboard/general.twig');
      echo $tpl->render([
         'path' => $_SERVER['REQUEST_URI'],
         'stats' => $statistics,
         'dates' => $dates
      ]);
   }

   public function getWeekStartDate() {
      $dto = new \DateTime();
      $dto->setISODate(date('Y'), date('W'));
      $weekStart = $dto->format('Y-m-d');
      return strtotime($weekStart);
   }

   public function rankAgencies($sortValue){
      if($sortValue == 'sold'){
         $query = "SELECT estate_agencies.name, Count(*) as count FROM residences INNER JOIN estate_agencies ON estate_agencies_id = estate_agencies.id WHERE blocked = 0 AND sold = 1 GROUP BY estate_agencies_id ORDER BY count DESC, estate_agencies.name";
      }
      else if($sortValue == 'unsold'){
         $query = "SELECT estate_agencies.name, Count(*) as count FROM residences INNER JOIN estate_agencies ON estate_agencies_id = estate_agencies.id WHERE blocked = 0 AND sold = 0 GROUP BY estate_agencies_id ORDER BY count DESC, estate_agencies.name";
      }
      else {//all properties
         $query = "SELECT estate_agencies.name, Count(*) as count FROM residences INNER JOIN estate_agencies ON estate_agencies_id = estate_agencies.id WHERE blocked = 0 GROUP BY estate_agencies_id ORDER BY count DESC, estate_agencies.name";
      }
      $stmt = $this->connection->prepare($query);
      $result = $stmt->executeQuery([]);
      $agencies = $result->fetchAllAssociative();
      
      
      $tpl = $this->twig->load('dashboard/rankAgencies.twig');
      echo $tpl->render([
         'sortValue' => $sortValue,
         'agencies' => $agencies,
         'path' => $_SERVER['REQUEST_URI']
      ]);
   }

   public function rankResidences(){
      $query = "SELECT residences.id, Count(*) as count FROM residences RIGHT JOIN user_favourites ON residences.id = residences_id GROUP BY residences_id ORDER BY count DESC, residences.id";
      $stmt = $this->connection->prepare($query);
      $result = $stmt->executeQuery([]);
      $residences = $result->fetchAllAssociative();
      
      
      $tpl = $this->twig->load('dashboard/rankResidences.twig');
      echo $tpl->render([
         'residences' => $residences,
         'path' => $_SERVER['REQUEST_URI']
      ]);
   }

   public function searchAgency(){
      $citiesDB = $this->connection->executeQuery('SELECT DISTINCT city FROM addresses')->fetchAllAssociative();
      $cities = [];
      foreach ($citiesDB as $value) {
         $cities[] = $value['city'];
      }

      $name = isset($_GET['name']) ? $_GET['name'] : '';
      $city = isset($_GET['city']) ? $_GET['city'] : '';

      $agencies = [];
      if($name !== ''){
         if(in_array($_GET['city'], $cities)){
            $query = "SELECT estate_agencies.*, estate_agencies.images AS image, addresses.city FROM estate_agencies INNER JOIN addresses ON estate_agencies.addresses_id = addresses.id WHERE addresses.city = ? AND estate_agencies.name LIKE ?";
            $stmt = $this->connection->prepare($query);
            $result = $stmt->executeQuery([$_GET['city'],'%'.$_GET['name'].'%']);
            $agencies = $result->fetchAllAssociative();
         }
         else{
            $query = "SELECT estate_agencies.*, estate_agencies.images AS image, addresses.city FROM estate_agencies INNER JOIN addresses ON estate_agencies.addresses_id = addresses.id WHERE estate_agencies.name LIKE ?";
            $stmt = $this->connection->prepare($query);
            $result = $stmt->executeQuery(['%'.$_GET['name'].'%']);
            $agencies = $result->fetchAllAssociative();
         }
      }
      
      $tpl = $this->twig->load('dashboard/searchAgency.twig');
      echo $tpl->render([
         'agencies' => $agencies,
         'search' => ['name' => $name, 'city' => $city],
         'cities' => $cities,
         'path' => $_SERVER['REQUEST_URI']
      ]);
   }

   public function blockedAgencies(){
      $query = "SELECT estate_agencies.*, estate_agencies.images AS image, addresses.city FROM estate_agencies INNER JOIN addresses ON estate_agencies.addresses_id = addresses.id WHERE blocked = 1";
      $stmt = $this->connection->prepare($query);
      $result = $stmt->executeQuery([]);
      $agencies = $result->fetchAllAssociative();
      
      $tpl = $this->twig->load('dashboard/blockedAgencies.twig');
      echo $tpl->render([
         'agencies' => $agencies,
         'path' => $_SERVER['REQUEST_URI']
      ]);
   }

   public function blockAgency($id){
      $query = "UPDATE estate_agencies SET blocked = 1 WHERE id = ?";
      $stmt = $this->connection->prepare($query);
      $result = $stmt->executeStatement([$id]);

      $path = "Location:".'../../..'.$_POST['path'];
      header($path);
   }

   public function unblockAgency($id){
      $query = "UPDATE estate_agencies SET blocked = 0 WHERE id = ?";
      $stmt = $this->connection->prepare($query);
      $result = $stmt->executeStatement([$id]);

      $path = "Location:".'../../..'.$_POST['path'];
      header($path);
   }

   public function removeAgency($id){
      $query = "DELETE FROM estate_agencies WHERE id = ?";
      $stmt = $this->connection->prepare($query);
      $result = $stmt->executeStatement([$id]);

      $path = "Location:".'../../..'.$_POST['path'];
      header($path);
   }
}
?>