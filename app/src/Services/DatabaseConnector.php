<?php

namespace Services;

use DateTime;
use SplFileObject;

class DatabaseConnector
{
    static function getConnection() : \Doctrine\DBAL\Connection {
        $connectionParams = [
            'url' => 'mysql://' . DB_USER . ':' . DB_PASS . '@' . DB_HOST . '/' . DB_NAME
        ];

        try {
            $connection = \Doctrine\DBAL\DriverManager::getConnection($connectionParams);
            $connection->connect();
        } catch (\Doctrine\DBAL\Exception $e) {
            self::handleError($e->getMessage());
            exit();
        }
        return $connection;
    }

    public static function handleError($message){
        if(DEBUG) echo($message . PHP_EOL);
        else {
            $logFile = new SplFileObject('../../storage/db.log', 'a+');
            $logFile->fwrite(PHP_EOL . date_format(new DateTime(), "d-m-Y H:i:s") . "\t" . $message);
            fclose($logFile);
            header('Location: /takenlijst/error.html');
        }
        
    }

}