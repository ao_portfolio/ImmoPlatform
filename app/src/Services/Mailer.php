<?php

namespace Services;

require_once (dirname(dirname(dirname(__FILE__))) . '/config/mailer.php');

class Mailer {
    protected \Swift_Mailer $mailer;

    public function __construct() {
        // Create the Transport
        $transport = (new \Swift_SmtpTransport(SMTP, PORT))
        ->setUsername(USERNAME_J)
        ->setPassword(PASSWORD_J)
        ;

        // Create the Mailer using your created Transport
        $this->mailer = new \Swift_Mailer($transport);
    }

    public function send($to, $body, $subject = 'Welcome to Alela!') {
        // Create a message to user
        $mail = (new \Swift_Message($subject))
        ->setFrom(['783452f5cd-2019e1@inbox.mailtrap.io' => 'Alela.no-reply'])
        ->setTo([$to['email'] => $to['username']])
        ->setBody($body, 'text/html');

        $this->mailer->send($mail);
    }

}
?>




