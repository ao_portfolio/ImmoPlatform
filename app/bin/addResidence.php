<?php
require_once ( __DIR__ . '/../vendor/autoload.php');
require_once (__DIR__ . '/../config/database.php');
require_once (__DIR__ . '/../src/Services/DatabaseConnector.php');

$types = ["Apartment", "Cottage", "Penthouse", "Mansion", "Single-family", "Multi-family", "Bungalow", "Townhouse"];

// connect to database
$connection = \Services\DatabaseConnector::getConnection();

//---add residence with some basic info----------------------------------------------
$stmt = $connection->prepare('INSERT INTO residences(type, price, name, description, city, country, zip_code) VALUES (?,?,?,?,?,?,?);');
$result = $stmt->executeStatement(["Apartment",100000,"The Smith Residence", "A nice apartment with an even better view", "Detroit", "usa", "90120"]);
$residenceId = $connection->lastInsertId();

//---add properties to that residence------------------------------------------------
$countableProperties = [
   ["Bathrooms", 2],
   ["Bedrooms", 1],
   ["Garage", 1]
];

$sizeProperties = [
   ["Garden", 10, 5],
   ["Living room", 6, 6]
];

$stmt = $connection->prepare('INSERT INTO properties(name, count) VALUES (?,?);');
$stmt2 = $connection->prepare('INSERT INTO properties(name, width, length) VALUES (?,?,?);');
$link = $connection->prepare('INSERT INTO residence_has_property(residences_id, properties_id) VALUES (?,?)');

foreach($countableProperties as $property){
   $result = $stmt->executeStatement([$property[0], $property[1]]);
   $result2 = $link->executeStatement([$residenceId, $connection->lastInsertId()]);
}
foreach($sizeProperties as $property){
   $result = $stmt2->executeStatement([$property[0], $property[1], $property[2]]);
   $result2 = $link->executeStatement([$residenceId, $connection->lastInsertId()]);
}




?>