<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* partials/footer.twig */
class __TwigTemplate_9cca7453fc3fa1a201be9c8fb66ba40de6f63f5953f47b6808d52e7c47f09f76 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<!--/ footer Star /-->
  <section class=\"section-footer\">
    <div class=\"container\">
      <div class=\"row\">
        <div class=\"col-sm-12 col-md-4\">
          <div class=\"widget-a\">
            <div class=\"w-header-a\">
              <h3 class=\"w-title-a text-brand\">EstateAgency</h3>
            </div>
            <div class=\"w-footer-a\">
              <ul class=\"list-unstyled\">
                <li class=\"color-a\">
                  <span class=\"color-text-a\">Email .</span> contact@estateagency.com</li>
                <li class=\"color-a\">
                  <span class=\"color-text-a\">Phone .</span> +32 412345678</li>
              </ul>
            </div>
          </div>
        </div>
        <div class=\"col-sm-12 col-md-4 section-md-t3\">
          <div class=\"widget-a\">
            <div class=\"w-header-a\">
              <h3 class=\"w-title-a text-brand\">The Company</h3>
            </div>
            <div class=\"w-body-a\">
              <div class=\"w-body-a\">
                <ul class=\"list-unstyled\">
                  <li class=\"item-list-a\">
                    <i class=\"fa fa-angle-right\"></i> <a href=\"/agents\">Agents</a>
                  </li>
                </ul>
              </div>
            </div>
          </div>
        </div>
        <div class=\"col-sm-12 col-md-4 section-md-t3\">
          <div class=\"widget-a\">
            <div class=\"w-header-a\">
              <h3 class=\"w-title-a text-brand\">International sites</h3>
            </div>
            <div class=\"w-body-a\">
              <ul class=\"list-unstyled\">
                <li class=\"item-list-a\">
                  <i class=\"fa fa-angle-right\"></i> <a href=\"/\">Belgium</a>
                </li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <footer>
    <div class=\"container\">
      <div class=\"row\">
        <div class=\"col-md-12\">
          <nav class=\"nav-footer\">
            <ul class=\"list-inline\">
              <li class=\"list-inline-item\">
                <a href=\"/\">Home</a>
              </li>
              <li class=\"list-inline-item\">
                <a href=\"/properties/page/1\">Property</a>
              </li>
              <li class=\"list-inline-item\">
                <a href=\"/agents\">Agents</a>
              </li>
            </ul>
          </nav>
          <div class=\"socials-a\">
            <ul class=\"list-inline\">
              <li class=\"list-inline-item\">
                <a href=\"#\">
                  <i class=\"fa fa-facebook\" aria-hidden=\"true\"></i>
                </a>
              </li>
              <li class=\"list-inline-item\">
                <a href=\"#\">
                  <i class=\"fa fa-twitter\" aria-hidden=\"true\"></i>
                </a>
              </li>
              <li class=\"list-inline-item\">
                <a href=\"#\">
                  <i class=\"fa fa-instagram\" aria-hidden=\"true\"></i>
                </a>
              </li>
              <li class=\"list-inline-item\">
                <a href=\"#\">
                  <i class=\"fa fa-pinterest-p\" aria-hidden=\"true\"></i>
                </a>
              </li>
              <li class=\"list-inline-item\">
                <a href=\"#\">
                  <i class=\"fa fa-dribbble\" aria-hidden=\"true\"></i>
                </a>
              </li>
            </ul>
          </div>
          <div class=\"copyright-footer\">
            <p class=\"copyright color-text-a\">
              &copy; Copyright
              <span class=\"color-a\">EstateAgency</span> All Rights Reserved.
            </p>
          </div>
          <div class=\"credits\">
            <!--
              All the links in the footer should remain intact.
              You can delete the links only if you purchased the pro version.
              Licensing information: https://bootstrapmade.com/license/
              Purchase the pro version with working PHP/AJAX contact form: https://bootstrapmade.com/buy/?theme=EstateAgency
            -->
            Designed by <a href=\"https://bootstrapmade.com/\">BootstrapMade</a>
          </div>
        </div>
      </div>
    </div>
  </footer>
  <!--/ Footer End /-->";
    }

    public function getTemplateName()
    {
        return "partials/footer.twig";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "partials/footer.twig", "/var/www/resources/templates/partials/footer.twig");
    }
}
