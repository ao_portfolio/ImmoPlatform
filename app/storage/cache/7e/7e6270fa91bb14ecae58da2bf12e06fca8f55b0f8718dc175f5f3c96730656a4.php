<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* registration.twig */
class __TwigTemplate_83a68490dbf5365c710d3ff15fb76edf4aa47715622e7a8dca99bb115b627abf extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<!DOCTYPE html>
<html lang=\"en\">
<head>
    <meta charset=\"utf-8\">
    <title>EstateAgency Bootstrap Template</title>
    <meta content=\"width=device-width, initial-scale=1.0\" name=\"viewport\">
    <meta content=\"\" name=\"keywords\">
    <meta content=\"\" name=\"description\">

    <!-- Favicons -->
    <link href=\"/img/favicon.png\" rel=\"icon\">
    <link href=\"/img/apple-touch-icon.png\" rel=\"apple-touch-icon\">

    <!-- Google Fonts -->
    <link href=\"https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700\" rel=\"stylesheet\">

    <!-- Bootstrap CSS File -->
    <link href=\"/lib/bootstrap/css/bootstrap.min.css\" rel=\"stylesheet\">

    <!-- Libraries CSS Files -->
    <link href=\"/lib/font-awesome/css/font-awesome.min.css\" rel=\"stylesheet\">
    <link href=\"/lib/animate/animate.min.css\" rel=\"stylesheet\">
    <link href=\"/lib/ionicons/css/ionicons.min.css\" rel=\"stylesheet\">
    <link href=\"/lib/owlcarousel/assets/owl.carousel.min.css\" rel=\"stylesheet\">

    <!-- Main Stylesheet File -->
    <link href=\"/css/style.css\" rel=\"stylesheet\">

    <!-- =======================================================
      Theme Name: EstateAgency
      Theme URL: https://bootstrapmade.com/real-estate-agency-bootstrap-template/
      Author: BootstrapMade.com
      License: https://bootstrapmade.com/license/
    ======================================================= -->
    <meta charset=\"UTF-8\">
    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">
    <!--===============================================================================================-->
    <link rel=\"icon\" type=\"image/png\" href=\"/img/icons/favicon.ico\"/>
    <!--===============================================================================================-->
    <link rel=\"stylesheet\" type=\"text/css\" href=\"/css/bootstrap.min.css\">
    <!--===============================================================================================-->
    <link rel=\"stylesheet\" type=\"text/css\" href=\"/fonts/font-awesome-4.7.0/css/font-awesome.min.css\">
    <!--===============================================================================================-->
    <link rel=\"stylesheet\" type=\"text/css\" href=\"/fonts/Linearicons-Free-v1.0.0/icon-font.min.css\">
    <!--===============================================================================================-->
    <link rel=\"stylesheet\" type=\"text/css\" href=\"/css/animate.css\">
    <!--===============================================================================================-->
    <link rel=\"stylesheet\" type=\"text/css\" href=\"/css/hamburgers.min.css\">
    <!--===============================================================================================-->
    <link rel=\"stylesheet\" type=\"text/css\" href=\"/css/animsition.min.css\">
    <!--===============================================================================================-->
    <link rel=\"stylesheet\" type=\"text/css\" href=\"/css/select2.min.css\">
    <!--===============================================================================================-->
    <link rel=\"stylesheet\" type=\"text/css\" href=\"/css/daterangepicker.css\">
    <!--===============================================================================================-->
    <link rel=\"stylesheet\" type=\"text/css\" href=\"/css/util.css\">
    <link rel=\"stylesheet\" type=\"text/css\" href=\"/css/main.css\">
    <!--===============================================================================================-->
</head>

<body>

<div class=\"click-closed\"></div>
<!--/ Form Search Star /-->
<div class=\"box-collapse\">
    <div class=\"title-box-d\">
        <h3 class=\"title-d\">Search Property</h3>
    </div>
    <span class=\"close-box-collapse right-boxed ion-ios-close\"></span>
    <div class=\"box-collapse-wrap form\">
        <form class=\"form-a\">
            <div class=\"row\">
                <div class=\"col-md-12 mb-2\">
                    <div class=\"form-group\">
                        <label for=\"Type\">Keyword</label>
                        <input type=\"text\" class=\"form-control form-control-lg form-control-a\" placeholder=\"Keyword\">
                    </div>
                </div>
                <div class=\"col-md-6 mb-2\">
                    <div class=\"form-group\">
                        <label for=\"Type\">Type</label>
                        <select class=\"form-control form-control-lg form-control-a\" id=\"Type\">
                            <option>All Type</option>
                            <option>For Rent</option>
                            <option>For Sale</option>
                            <option>Open House</option>
                        </select>
                    </div>
                </div>
                <div class=\"col-md-6 mb-2\">
                    <div class=\"form-group\">
                        <label for=\"city\">City</label>
                        <select class=\"form-control form-control-lg form-control-a\" id=\"city\">
                            <option>All City</option>
                            <option>Alabama</option>
                            <option>Arizona</option>
                            <option>California</option>
                            <option>Colorado</option>
                        </select>
                    </div>
                </div>
                <div class=\"col-md-6 mb-2\">
                    <div class=\"form-group\">
                        <label for=\"bedrooms\">Bedrooms</label>
                        <select class=\"form-control form-control-lg form-control-a\" id=\"bedrooms\">
                            <option>Any</option>
                            <option>01</option>
                            <option>02</option>
                            <option>03</option>
                        </select>
                    </div>
                </div>
                <div class=\"col-md-6 mb-2\">
                    <div class=\"form-group\">
                        <label for=\"garages\">Garages</label>
                        <select class=\"form-control form-control-lg form-control-a\" id=\"garages\">
                            <option>Any</option>
                            <option>01</option>
                            <option>02</option>
                            <option>03</option>
                            <option>04</option>
                        </select>
                    </div>
                </div>
                <div class=\"col-md-6 mb-2\">
                    <div class=\"form-group\">
                        <label for=\"bathrooms\">Bathrooms</label>
                        <select class=\"form-control form-control-lg form-control-a\" id=\"bathrooms\">
                            <option>Any</option>
                            <option>01</option>
                            <option>02</option>
                            <option>03</option>
                        </select>
                    </div>
                </div>
                <div class=\"col-md-6 mb-2\">
                    <div class=\"form-group\">
                        <label for=\"price\">Min Price</label>
                        <select class=\"form-control form-control-lg form-control-a\" id=\"price\">
                            <option>Unlimite</option>
                            <option>\$50,000</option>
                            <option>\$100,000</option>
                            <option>\$150,000</option>
                            <option>\$200,000</option>
                        </select>
                    </div>
                </div>
                <div class=\"col-md-12\">
                    <button type=\"submit\" class=\"btn btn-b\">Search Property</button>
                </div>
            </div>
        </form>
    </div>
</div>
<!--/ Form Search End /-->

";
        // line 157
        $this->loadTemplate("partials/header.twig", "registration.twig", 157)->display($context);
        // line 158
        echo "
<div class=\"limiter\">
    <div class=\"container-login100\" style=\"background-image: url('/img/bg-01.jpg');\">
        <div class=\"wrap-login100 p-t-30 p-b-50\">
\t\t\t\t<span class=\"login100-form-title p-b-41\">
\t\t\t\t\tAccount Registratie
\t\t\t\t</span>
            ";
        // line 165
        if (($context["formErrors"] ?? null)) {
            // line 166
            echo "                <div class=\"alert alert-danger\">
                    <strong>Hier is iets misgegaan.</strong>
                    <br><br>
                    <ul>
                        ";
            // line 170
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["formErrors"] ?? null));
            foreach ($context['_seq'] as $context["_key"] => $context["error"]) {
                // line 171
                echo "                            <li>";
                echo twig_escape_filter($this->env, $context["error"], "html", null, true);
                echo "</li>
                        ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['error'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 173
            echo "                    </ul>
                </div>
            ";
        }
        // line 176
        echo "
            <form action=\"registration\" method=\"POST\" class=\"login100-form p-b-33 p-t-5\">
                ";
        // line 179
        echo "                <div class=\"wrap-input100\">
                    <input class=\"input100\" type=\"text\" name=\"username\" placeholder=\"User name\" value=\"";
        // line 180
        echo twig_escape_filter($this->env, ($context["user"] ?? null), "html", null, true);
        echo "\">
                    <span class=\"focus-input100\" data-placeholder=\"&#xe82a;\"></span>
                </div>

                <div class=\"wrap-input100\">
                    <input class=\"input100\" type=\"text\" name=\"name\" placeholder=\"First name\" value=\"";
        // line 185
        echo twig_escape_filter($this->env, ($context["fname"] ?? null), "html", null, true);
        echo "\">
                    <span class=\"focus-input100\" data-placeholder=\"&#xe82a;\"></span>
                </div>

                <div class=\"wrap-input100\">
                    <input class=\"input100\" type=\"text\" name=\"lastname\" placeholder=\"Last name\" value=\"";
        // line 190
        echo twig_escape_filter($this->env, ($context["lname"] ?? null), "html", null, true);
        echo "\">
                    <span class=\"focus-input100\" data-placeholder=\"&#xe82a;\"></span>
                </div>

                <div class=\"wrap-input100\" >
                    <input class=\"input100\" type=\"text\" name=\"mail\" placeholder=\"E-mail\" value=\"";
        // line 195
        echo twig_escape_filter($this->env, ($context["mail"] ?? null), "html", null, true);
        echo "\">
                    <span class=\"focus-input100\" data-placeholder=\"&#xe82a;\"></span>
                </div>

                <div class=\"wrap-input100\">
                    <input class=\"input100\" type=\"text\" name=\"number\" placeholder=\"Phone number\" value=\"";
        // line 200
        echo twig_escape_filter($this->env, ($context["number"] ?? null), "html", null, true);
        echo "\">
                    <span class=\"focus-input100\" data-placeholder=\"&#xe82a;\"></span>
                </div>

                <div class=\"wrap-input100\">
                    <input class=\"input100\" type=\"password\" name=\"pass\" placeholder=\"Password\">
                    <span class=\"focus-input100\" data-placeholder=\"&#xe80f;\"></span>
                </div>

                <input type=\"hidden\" name=\"moduleAction\" value=\"regis\" />
                <div class=\"container-login100-form-btn m-t-32\">
                    <button class=\"login100-form-btn\">
                        Registreer
                    </button>
                </div>
                <p class=\"text-left\"><a href=\"login\">Reeds een account, log dan in!</a></p>

            </form>
        </div>
    </div>
</div>


<div id=\"dropDownSelect1\"></div>

<!--===============================================================================================-->
<script src=\"/js/jquery-3.2.1.min.js\"></script>
<!--===============================================================================================-->
<script src=\"/js/animsition.min.js\"></script>
<!--===============================================================================================-->
<script src=\"/js/popper.js\"></script>
<script src=\"/js/bootstrap.min.js\"></script>
<!--===============================================================================================-->
<script src=\"/js/select2.min.js\"></script>
<!--===============================================================================================-->
<script src=\"/js/moment.min.js\"></script>
<script src=\"/js/daterangepicker.js\"></script>
<!--===============================================================================================-->
<script src=\"/js/countdowntime.js\"></script>
<!--===============================================================================================-->
<script src=\"/js/mainlogin.js\"></script>
</body>
<a href=\"#\" class=\"back-to-top\"><i class=\"fa fa-chevron-up\"></i></a>
<div id=\"preloader\"></div>

";
        // line 245
        $this->loadTemplate("partials/libraries.twig", "registration.twig", 245)->display($context);
        // line 246
        echo "</html>";
    }

    public function getTemplateName()
    {
        return "registration.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  321 => 246,  319 => 245,  271 => 200,  263 => 195,  255 => 190,  247 => 185,  239 => 180,  236 => 179,  232 => 176,  227 => 173,  218 => 171,  214 => 170,  208 => 166,  206 => 165,  197 => 158,  195 => 157,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "registration.twig", "/var/www/resources/templates/registration.twig");
    }
}
