<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* partials/header.twig */
class __TwigTemplate_847d949929612597c610a7dbb4594fbacf9f204053c35367e1292c27356061ff extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<!--/ Nav Star /-->
  <nav class=\"navbar navbar-default navbar-trans navbar-expand-lg fixed-top\">
    <div class=\"container\">
      <button class=\"navbar-toggler collapsed\" type=\"button\" data-toggle=\"collapse\" data-target=\"#navbarDefault\"
        aria-controls=\"navbarDefault\" aria-expanded=\"false\" aria-label=\"Toggle navigation\">
        <span></span>
        <span></span>
        <span></span>
      </button>
      <a class=\"navbar-brand text-brand\" href=\"index.php\">Estate<span class=\"color-b\">Agency</span></a>
      <button type=\"button\" class=\"btn btn-link nav-search navbar-toggle-box-collapse d-md-none\" data-toggle=\"collapse\"
        data-target=\"#navbarTogglerDemo01\" aria-expanded=\"false\">
        <span class=\"fa fa-search\" aria-hidden=\"true\"></span>
      </button>
      <div class=\"navbar-collapse collapse justify-content-center\" id=\"navbarDefault\">
        <ul class=\"navbar-nav\">
          <li class=\"nav-item\">
            <a class=\"nav-link\" href=\"index.php\">Home</a>
          </li>
          <li class=\"nav-item\">
            <a class=\"nav-link\" href=\"about.html\">About</a>
          </li>
          <li class=\"nav-item\">
            <a class=\"nav-link\" href=\"property-grid.php\">Property</a>
          </li>
          <li class=\"nav-item\">
            <a class=\"nav-link\" href=\"blog-grid.html\">Blog</a>
          </li>
          <li class=\"nav-item dropdown\">
            <a class=\"nav-link dropdown-toggle\" href=\"#\" id=\"navbarDropdown\" role=\"button\" data-toggle=\"dropdown\"
              aria-haspopup=\"true\" aria-expanded=\"false\">
              Pages
            </a>
            <div class=\"dropdown-menu\" aria-labelledby=\"navbarDropdown\">
              <a class=\"dropdown-item\" href=\"property-single.html\">Property Single</a>
              <a class=\"dropdown-item\" href=\"blog-single.html\">Blog Single</a>
              <a class=\"dropdown-item\" href=\"agents.twig\">Agents Grid</a>
              <a class=\"dropdown-item\" href=\"agent-single.html\">Agent Single</a>
            </div>
          </li>
          <li class=\"nav-item\">
            <a class=\"nav-link\" href=\"contact.html\">Contact</a>
          </li>
          <li class=\"nav-item\">
            <a class=\"nav-link\" href=\"login.php\">Login</a>
          </li>
        </ul>
      </div>
      <button type=\"button\" class=\"btn btn-b-n navbar-toggle-box-collapse d-none d-md-block\" data-toggle=\"collapse\"
        data-target=\"#navbarTogglerDemo01\" aria-expanded=\"false\">
        <span class=\"fa fa-search\" aria-hidden=\"true\"></span>
      </button>
    </div>
  </nav>
  <!--/ Nav End /-->";
    }

    public function getTemplateName()
    {
        return "partials/header.twig";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "partials/header.twig", "/var/www/resources/templates/partials/header.twig");
    }
}
