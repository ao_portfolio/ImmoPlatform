<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* partials/header.twig */
class __TwigTemplate_566451040ff1257fee04890c5648d0b95c4abdde8ab148c5d84d80fb90044159 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<!--/ Nav Star /-->
  <nav class=\"navbar navbar-default navbar-trans navbar-expand-lg fixed-top\">
    <div class=\"container\">
      <button class=\"navbar-toggler collapsed\" type=\"button\" data-toggle=\"collapse\" data-target=\"#navbarDefault\"
        aria-controls=\"navbarDefault\" aria-expanded=\"false\" aria-label=\"Toggle navigation\">
        <span></span>
        <span></span>
        <span></span>
      </button>
      <a class=\"navbar-brand text-brand\" href=\"/\">Estate<span class=\"color-b\">Agency</span></a>
      <button type=\"button\" class=\"btn btn-link nav-search navbar-toggle-box-collapse d-md-none\" data-toggle=\"collapse\"
        data-target=\"#navbarTogglerDemo01\" aria-expanded=\"false\">
        <span class=\"fa fa-search\" aria-hidden=\"true\"></span>
      </button>
      <div class=\"navbar-collapse collapse justify-content-center\" id=\"navbarDefault\">
        <ul class=\"navbar-nav\">
          <li class=\"nav-item\">
            <a class=\"nav-link\" href=\"/\">Home</a>
          </li>
          <li class=\"nav-item\">
            <a class=\"nav-link\" href=\"/residences/page/1\">Property</a>
          </li>
          <li class=\"nav-item dropdown\">
            <a class=\"nav-link dropdown-toggle\" href=\"#\" id=\"navbarDropdown\" role=\"button\" data-toggle=\"dropdown\"
              aria-haspopup=\"true\" aria-expanded=\"false\">
              Pages
            </a>
            <div class=\"dropdown-menu\" aria-labelledby=\"navbarDropdown\">
              <a class=\"dropdown-item\" href=\"/agents\">Agents Grid</a>
              ";
        // line 30
        if (($context["user"] ?? null)) {
            // line 31
            echo "              <a class=\"dropdown-item\" href=\"/residences/favourites/page/1\">Favourites</a>
              <a class=\"dropdown-item\" href=\"/residences/preferences\">Preferences</a>
              ";
        }
        // line 34
        echo "            </div>
          </li>
          ";
        // line 36
        if ((twig_get_attribute($this->env, $this->source, ($context["user"] ?? null), "role", [], "any", false, false, false, 36) == "agent")) {
            // line 37
            echo "          <li class=\"nav-item\">
            <a class=\"nav-link\" href=\"/residences/add\">Add A Property</a>
          </li>
          ";
        }
        // line 41
        echo "          ";
        if ((twig_get_attribute($this->env, $this->source, ($context["user"] ?? null), "role", [], "any", false, false, false, 41) == "admin")) {
            // line 42
            echo "            <li class=\"nav-item\">
              <a class=\"nav-link\" href=\"/dashboard\">Dashboard</a>
            </li>
          ";
        }
        // line 46
        echo "          ";
        if (($context["user"] ?? null)) {
            // line 47
            echo "            <li class=\"nav-item\">
              <a class=\"nav-link\" href=\"/account\">My Account</a>
            </li>
            <li class=\"nav-item\">
              <a class=\"nav-link\" href=\"/logout\">Logout</a>
            </li>
          ";
        } else {
            // line 54
            echo "            <li class=\"nav-item\">
              <a class=\"nav-link\" href=\"/login\">Login</a>
            </li>
          ";
        }
        // line 58
        echo "
        </ul>
      </div>
      <button type=\"button\" class=\"btn btn-b-n navbar-toggle-box-collapse d-none d-md-block\" data-toggle=\"collapse\"
        data-target=\"#navbarTogglerDemo01\" aria-expanded=\"false\">
        <span class=\"fa fa-search\" aria-hidden=\"true\"></span>
      </button>
    </div>
  </nav>
  <!--/ Nav End /-->";
    }

    public function getTemplateName()
    {
        return "partials/header.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  114 => 58,  108 => 54,  99 => 47,  96 => 46,  90 => 42,  87 => 41,  81 => 37,  79 => 36,  75 => 34,  70 => 31,  68 => 30,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "partials/header.twig", "/var/www/resources/templates/partials/header.twig");
    }
}
