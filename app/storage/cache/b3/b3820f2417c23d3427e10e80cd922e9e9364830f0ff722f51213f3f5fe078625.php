<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* login.twig */
class __TwigTemplate_24bb5a2188aabe56586b2cd9d14a132c8a36d88f0eeffa0bc9c0102d0ae93bac extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<!DOCTYPE html>
<html lang=\"en\">
<head>
\t<meta charset=\"utf-8\">
\t<title>EstateAgency Bootstrap Template</title>
\t<meta content=\"width=device-width, initial-scale=1.0\" name=\"viewport\">
\t<meta content=\"\" name=\"keywords\">
\t<meta content=\"\" name=\"description\">

\t<!-- Favicons -->
\t<link href=\"/img/favicon.png\" rel=\"icon\">
\t<link href=\"/img/apple-touch-icon.png\" rel=\"apple-touch-icon\">

\t<!-- Google Fonts -->
\t<link href=\"https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700\" rel=\"stylesheet\">

\t<!-- Bootstrap CSS File -->
\t<link href=\"/lib/bootstrap/css/bootstrap.min.css\" rel=\"stylesheet\">

\t<!-- Libraries CSS Files -->
\t<link href=\"/lib/font-awesome/css/font-awesome.min.css\" rel=\"stylesheet\">
\t<link href=\"/lib/animate/animate.min.css\" rel=\"stylesheet\">
\t<link href=\"/lib/ionicons/css/ionicons.min.css\" rel=\"stylesheet\">
\t<link href=\"/lib/owlcarousel/assets/owl.carousel.min.css\" rel=\"stylesheet\">

\t<!-- Main Stylesheet File -->
\t<link href=\"/css/style.css\" rel=\"stylesheet\">

\t<!-- =======================================================
      Theme Name: EstateAgency
      Theme URL: https://bootstrapmade.com/real-estate-agency-bootstrap-template/
      Author: BootstrapMade.com
      License: https://bootstrapmade.com/license/
    ======================================================= -->
\t<meta charset=\"UTF-8\">
\t<meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">
\t<!--===============================================================================================-->
\t<link rel=\"icon\" type=\"image/png\" href=\"/img/icons/favicon.ico\"/>
\t<!--===============================================================================================-->
\t<link rel=\"stylesheet\" type=\"text/css\" href=\"/css/bootstrap.min.css\">
\t<!--===============================================================================================-->
\t<link rel=\"stylesheet\" type=\"text/css\" href=\"/fonts/font-awesome-4.7.0/css/font-awesome.min.css\">
\t<!--===============================================================================================-->
\t<link rel=\"stylesheet\" type=\"text/css\" href=\"/fonts/Linearicons-Free-v1.0.0/icon-font.min.css\">
\t<!--===============================================================================================-->
\t<link rel=\"stylesheet\" type=\"text/css\" href=\"/css/animate.css\">
\t<!--===============================================================================================-->
\t<link rel=\"stylesheet\" type=\"text/css\" href=\"/css/hamburgers.min.css\">
\t<!--===============================================================================================-->
\t<link rel=\"stylesheet\" type=\"text/css\" href=\"/css/animsition.min.css\">
\t<!--===============================================================================================-->
\t<link rel=\"stylesheet\" type=\"text/css\" href=\"/css/select2.min.css\">
\t<!--===============================================================================================-->
\t<link rel=\"stylesheet\" type=\"text/css\" href=\"/css/daterangepicker.css\">
\t<!--===============================================================================================-->
\t<link rel=\"stylesheet\" type=\"text/css\" href=\"/css/util.css\">
\t<link rel=\"stylesheet\" type=\"text/css\" href=\"/css/main.css\">
\t<!--===============================================================================================-->
</head>

<body>

<div class=\"click-closed\"></div>
<!--/ Form Search Star /-->
<div class=\"box-collapse\">
\t<div class=\"title-box-d\">
\t\t<h3 class=\"title-d\">Search Property</h3>
\t</div>
\t<span class=\"close-box-collapse right-boxed ion-ios-close\"></span>
\t<div class=\"box-collapse-wrap form\">
\t\t<form class=\"form-a\">
\t\t\t<div class=\"row\">
\t\t\t\t<div class=\"col-md-12 mb-2\">
\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t<label for=\"Type\">Keyword</label>
\t\t\t\t\t\t<input type=\"text\" class=\"form-control form-control-lg form-control-a\" placeholder=\"Keyword\">
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t\t<div class=\"col-md-6 mb-2\">
\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t<label for=\"Type\">Type</label>
\t\t\t\t\t\t<select class=\"form-control form-control-lg form-control-a\" id=\"Type\">
\t\t\t\t\t\t\t<option>All Type</option>
\t\t\t\t\t\t\t<option>For Rent</option>
\t\t\t\t\t\t\t<option>For Sale</option>
\t\t\t\t\t\t\t<option>Open House</option>
\t\t\t\t\t\t</select>
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t\t<div class=\"col-md-6 mb-2\">
\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t<label for=\"city\">City</label>
\t\t\t\t\t\t<select class=\"form-control form-control-lg form-control-a\" id=\"city\">
\t\t\t\t\t\t\t<option>All City</option>
\t\t\t\t\t\t\t<option>Alabama</option>
\t\t\t\t\t\t\t<option>Arizona</option>
\t\t\t\t\t\t\t<option>California</option>
\t\t\t\t\t\t\t<option>Colorado</option>
\t\t\t\t\t\t</select>
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t\t<div class=\"col-md-6 mb-2\">
\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t<label for=\"bedrooms\">Bedrooms</label>
\t\t\t\t\t\t<select class=\"form-control form-control-lg form-control-a\" id=\"bedrooms\">
\t\t\t\t\t\t\t<option>Any</option>
\t\t\t\t\t\t\t<option>01</option>
\t\t\t\t\t\t\t<option>02</option>
\t\t\t\t\t\t\t<option>03</option>
\t\t\t\t\t\t</select>
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t\t<div class=\"col-md-6 mb-2\">
\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t<label for=\"garages\">Garages</label>
\t\t\t\t\t\t<select class=\"form-control form-control-lg form-control-a\" id=\"garages\">
\t\t\t\t\t\t\t<option>Any</option>
\t\t\t\t\t\t\t<option>01</option>
\t\t\t\t\t\t\t<option>02</option>
\t\t\t\t\t\t\t<option>03</option>
\t\t\t\t\t\t\t<option>04</option>
\t\t\t\t\t\t</select>
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t\t<div class=\"col-md-6 mb-2\">
\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t<label for=\"bathrooms\">Bathrooms</label>
\t\t\t\t\t\t<select class=\"form-control form-control-lg form-control-a\" id=\"bathrooms\">
\t\t\t\t\t\t\t<option>Any</option>
\t\t\t\t\t\t\t<option>01</option>
\t\t\t\t\t\t\t<option>02</option>
\t\t\t\t\t\t\t<option>03</option>
\t\t\t\t\t\t</select>
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t\t<div class=\"col-md-6 mb-2\">
\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t<label for=\"price\">Min Price</label>
\t\t\t\t\t\t<select class=\"form-control form-control-lg form-control-a\" id=\"price\">
\t\t\t\t\t\t\t<option>Unlimite</option>
\t\t\t\t\t\t\t<option>\$50,000</option>
\t\t\t\t\t\t\t<option>\$100,000</option>
\t\t\t\t\t\t\t<option>\$150,000</option>
\t\t\t\t\t\t\t<option>\$200,000</option>
\t\t\t\t\t\t</select>
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t\t<div class=\"col-md-12\">
\t\t\t\t\t<button type=\"submit\" class=\"btn btn-b\">Search Property</button>
\t\t\t\t</div>
\t\t\t</div>
\t\t</form>
\t</div>
</div>
<!--/ Form Search End /-->

";
        // line 157
        $this->loadTemplate("partials/header.twig", "login.twig", 157)->display($context);
        // line 158
        echo "
<div class=\"limiter\">
\t<div class=\"container-login100\" style=\"background-image: url('/img/bg-01.jpg');\">
\t\t<div class=\"wrap-login100 p-t-30 p-b-50\">
\t\t\t\t<span class=\"login100-form-title p-b-41\">
\t\t\t\t\tAccount Login
\t\t\t\t</span>
\t\t\t";
        // line 165
        if (($context["formErrors"] ?? null)) {
            // line 166
            echo "\t\t\t\t<div class=\"alert alert-danger\">
\t\t\t\t\t<strong>Hier is iets misgegaan.</strong>
\t\t\t\t\t<br><br>
\t\t\t\t\t<ul>
\t\t\t\t\t\t";
            // line 170
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["formErrors"] ?? null));
            foreach ($context['_seq'] as $context["_key"] => $context["error"]) {
                // line 171
                echo "\t\t\t\t\t\t\t<li>";
                echo twig_escape_filter($this->env, $context["error"], "html", null, true);
                echo "</li>
\t\t\t\t\t\t";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['error'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 173
            echo "\t\t\t\t\t</ul>
\t\t\t\t</div>
\t\t\t";
        }
        // line 176
        echo "\t\t\t<form action=\"login\" method=\"POST\" class=\"login100-form validate-form p-b-33 p-t-5\">

\t\t\t\t<div class=\"wrap-input100 validate-input\" data-validate = \"Enter username\">
\t\t\t\t\t<input class=\"input100\" type=\"text\" name=\"username\" placeholder=\"User name\" value=\"";
        // line 179
        echo twig_escape_filter($this->env, ($context["name"] ?? null), "html", null, true);
        echo "\">
\t\t\t\t\t<span class=\"focus-input100\" data-placeholder=\"&#xe82a;\"></span>
\t\t\t\t</div>

\t\t\t\t<div class=\"wrap-input100 validate-input\" data-validate=\"Enter password\">
\t\t\t\t\t<input class=\"input100\" type=\"password\" name=\"pass\" placeholder=\"Password\">
\t\t\t\t\t<span class=\"focus-input100\" data-placeholder=\"&#xe80f;\"></span>
\t\t\t\t</div>

\t\t\t\t<input type=\"hidden\" name=\"moduleAction\" value=\"login\" />
\t\t\t\t<div class=\"container-login100-form-btn m-t-32\">
\t\t\t\t\t<button class=\"login100-form-btn\">
\t\t\t\t\t\tLogin
\t\t\t\t\t</button>
\t\t\t\t</div>
\t\t\t\t<p class=\"text-left\"><a href=\"registration\">Nog geen account, maak er één aan!</a></p>

\t\t\t</form>
\t\t</div>
\t</div>
</div>


<div id=\"dropDownSelect1\"></div>

<!--===============================================================================================-->
<script src=\"/js/jquery-3.2.1.min.js\"></script>
<!--===============================================================================================-->
<script src=\"/js/animsition.min.js\"></script>
<!--===============================================================================================-->
<script src=\"/js/popper.js\"></script>
<script src=\"/js/bootstrap.min.js\"></script>
<!--===============================================================================================-->
<script src=\"/js/select2.min.js\"></script>
<!--===============================================================================================-->
<script src=\"/js/moment.min.js\"></script>
<script src=\"/js/daterangepicker.js\"></script>
<!--===============================================================================================-->
<script src=\"/js/countdowntime.js\"></script>
<!--===============================================================================================-->
<script src=\"/js/mainlogin.js\"></script>
</body>
<a href=\"#\" class=\"back-to-top\"><i class=\"fa fa-chevron-up\"></i></a>
<div id=\"preloader\"></div>

";
        // line 224
        $this->loadTemplate("partials/libraries.twig", "login.twig", 224)->display($context);
        // line 225
        echo "
</html>";
    }

    public function getTemplateName()
    {
        return "login.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  287 => 225,  285 => 224,  237 => 179,  232 => 176,  227 => 173,  218 => 171,  214 => 170,  208 => 166,  206 => 165,  197 => 158,  195 => 157,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "login.twig", "/var/www/resources/templates/login.twig");
    }
}
