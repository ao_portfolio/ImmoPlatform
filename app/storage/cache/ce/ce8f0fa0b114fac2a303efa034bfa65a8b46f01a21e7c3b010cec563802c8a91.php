<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* layout.twig */
class __TwigTemplate_286c58f36a9c14be704e4a42fd25ec3b05abadb8dee908a44700e732b5230a15 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
            'pageContent' => [$this, 'block_pageContent'],
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<!DOCTYPE html>
<html lang=\"en\">
<head>
  <meta charset=\"utf-8\">
  <title>EstateAgency Bootstrap Template</title>
  <meta content=\"width=device-width, initial-scale=1.0\" name=\"viewport\">
  <meta content=\"\" name=\"keywords\">
  <meta content=\"\" name=\"description\">

  <!-- Favicons -->
  <link href=\"/img/favicon.png\" rel=\"icon\">
  <link href=\"/img/apple-touch-icon.png\" rel=\"apple-touch-icon\">

  <!-- Google Fonts -->
  <link href=\"https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700\" rel=\"stylesheet\">

  <!-- Bootstrap CSS File -->
  <link href=\"/lib/bootstrap/css/bootstrap.min.css\" rel=\"stylesheet\">

  <!-- Libraries CSS Files -->
  <link href=\"/lib/font-awesome/css/font-awesome.min.css\" rel=\"stylesheet\">
  <link href=\"/lib/animate/animate.min.css\" rel=\"stylesheet\">
  <link href=\"/lib/ionicons/css/ionicons.min.css\" rel=\"stylesheet\">
  <link href=\"/lib/owlcarousel/assets/owl.carousel.min.css\" rel=\"stylesheet\">
  <link href=\"//cdn.datatables.net/1.11.3/css/jquery.dataTables.min.css\" rel=\"stylesheet\">

  <!-- Main Stylesheet File -->
  <link href=\"/css/style.css\" rel=\"stylesheet\">

  <!-- =======================================================
    Theme Name: EstateAgency
    Theme URL: https://bootstrapmade.com/real-estate-agency-bootstrap-template/
    Author: BootstrapMade.com
    License: https://bootstrapmade.com/license/
  ======================================================= -->
</head>

<body>

  <div class=\"click-closed\"></div>
  <!--/ Form Search Star /-->
  <div class=\"box-collapse\">
    <div class=\"title-box-d\">
      <h3 class=\"title-d\">Search Property</h3>
    </div>
    <span class=\"close-box-collapse right-boxed ion-ios-close\"></span>
    <div class=\"box-collapse-wrap form\">
      <form class=\"form-a\">
        <div class=\"row\">
          <div class=\"col-md-12 mb-2\">
            <div class=\"form-group\">
              <label for=\"Type\">Keyword</label>
              <input type=\"text\" class=\"form-control form-control-lg form-control-a\" placeholder=\"Keyword\">
            </div>
          </div>
          <div class=\"col-md-6 mb-2\">
            <div class=\"form-group\">
              <label for=\"Type\">Type</label>
              <select class=\"form-control form-control-lg form-control-a\" id=\"Type\">
                <option>All Type</option>
                <option>For Rent</option>
                <option>For Sale</option>
                <option>Open House</option>
              </select>
            </div>
          </div>
          <div class=\"col-md-6 mb-2\">
            <div class=\"form-group\">
              <label for=\"city\">City</label>
              <select class=\"form-control form-control-lg form-control-a\" id=\"city\">
                <option>All City</option>
                <option>Alabama</option>
                <option>Arizona</option>
                <option>California</option>
                <option>Colorado</option>
              </select>
            </div>
          </div>
          <div class=\"col-md-6 mb-2\">
            <div class=\"form-group\">
              <label for=\"bedrooms\">Bedrooms</label>
              <select class=\"form-control form-control-lg form-control-a\" id=\"bedrooms\">
                <option>Any</option>
                <option>01</option>
                <option>02</option>
                <option>03</option>
              </select>
            </div>
          </div>
          <div class=\"col-md-6 mb-2\">
            <div class=\"form-group\">
              <label for=\"garages\">Garages</label>
              <select class=\"form-control form-control-lg form-control-a\" id=\"garages\">
                <option>Any</option>
                <option>01</option>
                <option>02</option>
                <option>03</option>
                <option>04</option>
              </select>
            </div>
          </div>
          <div class=\"col-md-6 mb-2\">
            <div class=\"form-group\">
              <label for=\"bathrooms\">Bathrooms</label>
              <select class=\"form-control form-control-lg form-control-a\" id=\"bathrooms\">
                <option>Any</option>
                <option>01</option>
                <option>02</option>
                <option>03</option>
              </select>
            </div>
          </div>
          <div class=\"col-md-6 mb-2\">
            <div class=\"form-group\">
              <label for=\"price\">Min Price</label>
              <select class=\"form-control form-control-lg form-control-a\" id=\"price\">
                <option>Unlimite</option>
                <option>\$50,000</option>
                <option>\$100,000</option>
                <option>\$150,000</option>
                <option>\$200,000</option>
              </select>
            </div>
          </div>
          <div class=\"col-md-12\">
            <button type=\"submit\" class=\"btn btn-b\">Search Property</button>
          </div>
        </div>
      </form>
    </div>
  </div>
  <!--/ Form Search End /-->

  ";
        // line 134
        $this->loadTemplate("partials/header.twig", "layout.twig", 134)->display($context);
        // line 135
        echo "
   ";
        // line 136
        $this->displayBlock('pageContent', $context, $blocks);
        // line 138
        echo "
  ";
        // line 139
        $this->loadTemplate("partials/footer.twig", "layout.twig", 139)->display($context);
        // line 140
        echo "
  <a href=\"#\" class=\"back-to-top\"><i class=\"fa fa-chevron-up\"></i></a>
  <div id=\"preloader\"></div>

  ";
        // line 144
        $this->loadTemplate("partials/libraries.twig", "layout.twig", 144)->display($context);
        // line 145
        echo "
</body>
</html>
";
    }

    // line 136
    public function block_pageContent($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 137
        echo "   ";
    }

    public function getTemplateName()
    {
        return "layout.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  204 => 137,  200 => 136,  193 => 145,  191 => 144,  185 => 140,  183 => 139,  180 => 138,  178 => 136,  175 => 135,  173 => 134,  38 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "layout.twig", "/var/www/resources/templates/layout.twig");
    }
}
