<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* partials/libraries.twig */
class __TwigTemplate_20be48f2979e6a91d6191a22b70a2c537b1859ca275a58552c9de469e3e5f8b1 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<!-- JavaScript Libraries -->
  <script src=\"/lib/jquery/jquery.min.js\"></script>
  <script src=\"/lib/jquery/jquery-migrate.min.js\"></script>
  <script src=\"/lib/popper/popper.min.js\"></script>
  <script src=\"/lib/bootstrap/js/bootstrap.min.js\"></script>
  <script src=\"/lib/easing/easing.min.js\"></script>
  <script src=\"/lib/owlcarousel/owl.carousel.min.js\"></script>
  <script src=\"/lib/scrollreveal/scrollreveal.min.js\"></script>
  <script src=\"https://kit.fontawesome.com/790e843194.js\" crossorigin=\"anonymous\"></script>
  <!-- Contact Form JavaScript File -->
  <script src=\"/contactform/contactform.js\"></script>

  <!-- Template Main Javascript File -->
  <script src=\"/js/main.js\"></script>

  <script src=\"/js/residences.js\"></script>";
    }

    public function getTemplateName()
    {
        return "partials/libraries.twig";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "partials/libraries.twig", "/var/www/resources/templates/partials/libraries.twig");
    }
}
