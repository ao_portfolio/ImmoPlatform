<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* property-grid.twig */
class __TwigTemplate_d9c83d7460ef2dbccbbb0554fd35fa0e9f1d3a0a86a17612932d9405ed0fb0eb extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'pageContent' => [$this, 'block_pageContent'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "layout.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $this->parent = $this->loadTemplate("layout.twig", "property-grid.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_pageContent($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 4
        echo "  <!--/ Intro Single star /-->
  <section class=\"intro-single\">
    <div class=\"container\">
      <div class=\"row\">
        <div class=\"col-md-12 col-lg-8\">
          <div class=\"title-single-box\">
            <h1 class=\"title-single\">Our Amazing Properties</h1>
            <span class=\"color-text-a\">Grid Properties</span>
          </div>
        </div>
        <div class=\"col-md-12 col-lg-4\">
          <nav aria-label=\"breadcrumb\" class=\"breadcrumb-box d-flex justify-content-lg-end\">
            <ol class=\"breadcrumb\">
              <li class=\"breadcrumb-item\">
                <a href=\"#\">Home</a>
              </li>
              <li class=\"breadcrumb-item active\" aria-current=\"page\">
                Properties Grid
              </li>
            </ol>
          </nav>
        </div>
      </div>
    </div>
  </section>
  <!--/ Intro Single End /-->

  <!--/ Property Grid Star /-->
  <section class=\"property-grid grid\">
    <div class=\"container\">
      <div class=\"row\">
        <div class=\"col-sm-12\">
          <div class=\"grid-option\">
            <form>
              <select class=\"custom-select\">
                <option selected>All</option>
                <option value=\"1\">New to Old</option>
                <option value=\"2\">For Rent</option>
                <option value=\"3\">For Sale</option>
              </select>
            </form>
          </div>
        </div>
        <!-- Start card -->
        ";
        // line 48
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["residences"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["residence"]) {
            // line 49
            echo "        <div class=\"col-md-4\">
          <div class=\"card-box-a card-shadow\">
            <div class=\"img-box-a\">
              <img src=\"img/property-1.jpg\" alt=\"\" class=\"img-a img-fluid\">
            </div>
            <div class=\"card-overlay\">
              <div class=\"card-overlay-a-content\">
                <div class=\"card-header-a\">
                  <h2 class=\"card-title-a\">
                    <a href=\"#\">";
            // line 58
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["residence"], "city", [], "any", false, false, false, 58), "html", null, true);
            echo ", ";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["residence"], "country", [], "any", false, false, false, 58), "html", null, true);
            echo "
                      <br /> Olive Road Two</a>
                  </h2>
                </div>
                <div class=\"card-body-a\">
                  <div class=\"price-box d-flex\">
                      <form action=\"index.php\" method=\"POST\">
                        <input type=\"hidden\" name=\"residence\" value=\"";
            // line 65
            echo twig_escape_filter($this->env, $context["residence"], "html", null, true);
            echo "\"></input>
                        <input type=\"hidden\" name=\"moduleAction\" value=\"single-residence\"></input>
                        <input class=\"price-a\" type=\"submit\" value=\"rent | € ";
            // line 67
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["residence"], "price", [], "any", false, false, false, 67), "html", null, true);
            echo "\"></input>
                      </form>
                  </div>
                </div>
                <div class=\"card-footer-a\">
                  <ul class=\"card-info d-flex justify-content-around\">
                    <li>
                      <h4 class=\"card-info-title\">Area</h4>
                      <span>340m
                        <sup>2</sup>
                      </span>
                    </li>
                    <li>
                      <h4 class=\"card-info-title\">Beds</h4>
                      <span>2</span>
                    </li>
                    <li>
                      <h4 class=\"card-info-title\">Baths</h4>
                      <span>4</span>
                    </li>
                    <li>
                      <h4 class=\"card-info-title\">Garages</h4>
                      <span>1</span>
                    </li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
        </div>
              ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['residence'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 98
        echo "        <!-- End card -->
      </div>
      <div class=\"row\">
        <div class=\"col-sm-12\">
          <nav class=\"pagination-a\">
            <ul class=\"pagination justify-content-end\">
              <li class=\"page-item disabled\">
                <a class=\"page-link\" href=\"#\" tabindex=\"-1\">
                  <span class=\"ion-ios-arrow-back\"></span>
                </a>
              </li>
              <li class=\"page-item\">
                <a class=\"page-link\" href=\"#\">1</a>
              </li>
              <li class=\"page-item active\">
                <a class=\"page-link\" href=\"#\">2</a>
              </li>
              <li class=\"page-item\">
                <a class=\"page-link\" href=\"#\">3</a>
              </li>
              <li class=\"page-item next\">
                <a class=\"page-link\" href=\"#\">
                  <span class=\"ion-ios-arrow-forward\"></span>
                </a>
              </li>
            </ul>
          </nav>
        </div>
      </div>
    </div>
  </section>
  <!--/ Property Grid End /-->
  ";
    }

    public function getTemplateName()
    {
        return "property-grid.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  165 => 98,  128 => 67,  123 => 65,  111 => 58,  100 => 49,  96 => 48,  50 => 4,  46 => 3,  35 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "property-grid.twig", "/var/www/resources/templates/property-grid.twig");
    }
}
