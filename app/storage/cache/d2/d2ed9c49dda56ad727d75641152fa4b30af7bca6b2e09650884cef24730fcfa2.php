<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* index.twig */
class __TwigTemplate_73415d0b48fd136b53658e1563cb93f20f3c45337569f756f6e8b4ee34ddeb6b extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'pageContent' => [$this, 'block_pageContent'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "layout.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $this->parent = $this->loadTemplate("layout.twig", "index.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_pageContent($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 4
        echo "  <!--/ Carousel Star /-->
  <div class=\"intro intro-carousel\">
    <div id=\"carousel\" class=\"owl-carousel owl-theme\">
      ";
        // line 7
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["residences"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["residence"]) {
            // line 8
            echo "      <div class=\"carousel-item-a intro-item bg-image\" style=\"background-image: url(/img/slide-2.jpg)\">
        <div class=\"overlay overlay-a\"></div>
        <div class=\"intro-content display-table\">
          <div class=\"table-cell\">
            <div class=\"container\">
              <div class=\"row\">
                <div class=\"col-lg-8\">
                  <div class=\"intro-body\">
                    <p class=\"intro-title-top\">";
            // line 16
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["residence"], "city", [], "any", false, false, false, 16), "html", null, true);
            echo ", ";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["residence"], "country", [], "any", false, false, false, 16), "html", null, true);
            echo "
                      <br> ";
            // line 17
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["residence"], "zip_code", [], "any", false, false, false, 17), "html", null, true);
            echo "</p>
                    <h1 class=\"intro-title mb-4\">
                      <span class=\"color-b\">";
            // line 19
            echo twig_escape_filter($this->env, twig_random($this->env, 1, 100), "html", null, true);
            echo " </span> Rino
                      <br> Venda Road Five</h1>
                    <p class=\"intro-subtitle intro-price\">
                      <form action=\"index.php\" method=\"POST\">
                        <input type=\"hidden\" name=\"residence\" value=\"";
            // line 23
            echo twig_escape_filter($this->env, $context["residence"], "html", null, true);
            echo "\"></input>
                        <input type=\"hidden\" name=\"moduleAction\" value=\"single-residence\"></input>
                        <input class=\"price-a\" type=\"submit\" value=\"rent | € ";
            // line 25
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["residence"], "price", [], "any", false, false, false, 25), "html", null, true);
            echo "\"></input>
                      </form>
                    </p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['residence'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 36
        echo "    </div>
  </div>
  <!--/ Carousel end /-->

  <!--/ Services Star /-->
  <section class=\"section-services section-t8\">
    <div class=\"container\">
      <div class=\"row\">
        <div class=\"col-md-12\">
          <div class=\"title-wrap d-flex justify-content-between\">
            <div class=\"title-box\">
              <h2 class=\"title-a\">Our Services</h2>
            </div>
          </div>
        </div>
      </div>
      <div class=\"row\">
        <div class=\"col-md-4\">
          <div class=\"card-box-c foo\">
            <div class=\"card-header-c d-flex\">
              <div class=\"card-box-ico\">
                <span class=\"fa fa-gamepad\"></span>
              </div>
              <div class=\"card-title-c align-self-center\">
                <h2 class=\"title-c\">Lifestyle</h2>
              </div>
            </div>
            <div class=\"card-body-c\">
              <p class=\"content-c\">
                Sed porttitor lectus nibh. Cras ultricies ligula sed magna dictum porta. Praesent sapien massa,
                convallis a pellentesque
                nec, egestas non nisi.
              </p>
            </div>
            <div class=\"card-footer-c\">
              <a href=\"#\" class=\"link-c link-icon\">Read more
                <span class=\"ion-ios-arrow-forward\"></span>
              </a>
            </div>
          </div>
        </div>
        <div class=\"col-md-4\">
          <div class=\"card-box-c foo\">
            <div class=\"card-header-c d-flex\">
              <div class=\"card-box-ico\">
                <span class=\"fa fa-usd\"></span>
              </div>
              <div class=\"card-title-c align-self-center\">
                <h2 class=\"title-c\">Loans</h2>
              </div>
            </div>
            <div class=\"card-body-c\">
              <p class=\"content-c\">
                Nulla porttitor accumsan tincidunt. Curabitur aliquet quam id dui posuere blandit. Mauris blandit
                aliquet elit, eget tincidunt
                nibh pulvinar a.
              </p>
            </div>
            <div class=\"card-footer-c\">
              <a href=\"#\" class=\"link-c link-icon\">Read more
                <span class=\"ion-ios-arrow-forward\"></span>
              </a>
            </div>
          </div>
        </div>
        <div class=\"col-md-4\">
          <div class=\"card-box-c foo\">
            <div class=\"card-header-c d-flex\">
              <div class=\"card-box-ico\">
                <span class=\"fa fa-home\"></span>
              </div>
              <div class=\"card-title-c align-self-center\">
                <h2 class=\"title-c\">Sell</h2>
              </div>
            </div>
            <div class=\"card-body-c\">
              <p class=\"content-c\">
                Sed porttitor lectus nibh. Cras ultricies ligula sed magna dictum porta. Praesent sapien massa,
                convallis a pellentesque
                nec, egestas non nisi.
              </p>
            </div>
            <div class=\"card-footer-c\">
              <a href=\"#\" class=\"link-c link-icon\">Read more
                <span class=\"ion-ios-arrow-forward\"></span>
              </a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!--/ Services End /-->

  <!--/ Property Star /-->
  <section class=\"section-property section-t8\">
    <div class=\"container\">
      <div class=\"row\">
        <div class=\"col-md-12\">
          <div class=\"title-wrap d-flex justify-content-between\">
            <div class=\"title-box\">
              <h2 class=\"title-a\">Latest Properties</h2>
            </div>
            <div class=\"title-link\">
              <a href=\"property-grid.php\">All Properties
                <span class=\"ion-ios-arrow-forward\"></span>
              </a>
            </div>
          </div>
        </div>
      </div>
      <div id=\"property-carousel\" class=\"owl-carousel owl-theme\">
        ";
        // line 148
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["latest_properties"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["residence"]) {
            // line 149
            echo "        <div class=\"carousel-item-b\">
          <div class=\"card-box-a card-shadow\">
            <div class=\"img-box-a\">
              <img src=\"/img/property-6.jpg\" alt=\"\" class=\"img-a img-fluid\">
            </div>
            <div class=\"card-overlay\">
              <div class=\"card-overlay-a-content\">
                <div class=\"card-header-a\">
                  <h2 class=\"card-title-a\">
                    <p>
                      ";
            // line 159
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["residence"], "zip_code", [], "any", false, false, false, 159), "html", null, true);
            echo ", ";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["residence"], "city", [], "any", false, false, false, 159), "html", null, true);
            echo " <br/> 
                      ";
            // line 160
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["residence"], "country", [], "any", false, false, false, 160), "html", null, true);
            echo "
                    </p>
                  </h2>
                </div>
                <div class=\"card-body-a\">
                  <div class=\"price-box d-flex\">
                    <form action=\"index.php\" method=\"POST\">
                        <input type=\"hidden\" name=\"residence\" value=\"";
            // line 167
            echo twig_escape_filter($this->env, $context["residence"], "html", null, true);
            echo "\"></input>
                        <input type=\"hidden\" name=\"moduleAction\" value=\"single-residence\"></input>
                        <input class=\"price-a\" type=\"submit\" value=\"rent | € ";
            // line 169
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["residence"], "price", [], "any", false, false, false, 169), "html", null, true);
            echo "\"></input>
                      </form>
                  </div>
                </div>
                <div class=\"card-footer-a\">
                  <ul class=\"card-info d-flex justify-content-around\">
                    <li>
                      <h4 class=\"card-info-title\">Area</h4>
                      <span>";
            // line 177
            echo twig_escape_filter($this->env, twig_random($this->env, 100, 700), "html", null, true);
            echo "m
                        <sup>2</sup>
                      </span>
                    </li>
                    <li>
                      <h4 class=\"card-info-title\">Beds</h4>
                      <span>";
            // line 183
            echo twig_escape_filter($this->env, twig_random($this->env, 1, 6), "html", null, true);
            echo "</span>
                    </li>
                    <li>
                      <h4 class=\"card-info-title\">Baths</h4>
                      <span>";
            // line 187
            echo twig_escape_filter($this->env, twig_random($this->env, 1, 4), "html", null, true);
            echo "</span>
                    </li>
                    <li>
                      <h4 class=\"card-info-title\">Garages</h4>
                      <span>";
            // line 191
            echo twig_escape_filter($this->env, twig_random($this->env, 2), "html", null, true);
            echo "</span>
                    </li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
        </div>
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['residence'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 200
        echo "      </div>
    </div>
  </section>
  <!--/ Property End /-->

  <!--/ Agents Star /-->
  <section class=\"section-agents section-t8\">
    <div class=\"container\">
      <div class=\"row\">
        <div class=\"col-md-12\">
          <div class=\"title-wrap d-flex justify-content-between\">
            <div class=\"title-box\">
              <h2 class=\"title-a\">Best Agents</h2>
            </div>
            <div class=\"title-link\">
              <a href=\"agents.twig\">All Agents
                <span class=\"ion-ios-arrow-forward\"></span>
              </a>
            </div>
          </div>
        </div>
      </div>
      <div class=\"row\">
        <div class=\"col-md-4\">
          <div class=\"card-box-d\">
            <div class=\"card-img-d\">
              <img src=\"/img/agent-4.jpg\" alt=\"\" class=\"img-d img-fluid\">
            </div>
            <div class=\"card-overlay card-overlay-hover\">
              <div class=\"card-header-d\">
                <div class=\"card-title-d align-self-center\">
                  <h3 class=\"title-d\">
                    <a href=\"agent-single.html\" class=\"link-two\">Margaret Sotillo
                      <br> Escala</a>
                  </h3>
                </div>
              </div>
              <div class=\"card-body-d\">
                <p class=\"content-d color-text-a\">
                  Sed porttitor lectus nibh, Cras ultricies ligula sed magna dictum porta two.
                </p>
                <div class=\"info-agents color-a\">
                  <p>
                    <strong>Phone: </strong> +54 356 945234</p>
                  <p>
                    <strong>Email: </strong> agents@example.com</p>
                </div>
              </div>
              <div class=\"card-footer-d\">
                <div class=\"socials-footer d-flex justify-content-center\">
                  <ul class=\"list-inline\">
                    <li class=\"list-inline-item\">
                      <a href=\"#\" class=\"link-one\">
                        <i class=\"fa fa-facebook\" aria-hidden=\"true\"></i>
                      </a>
                    </li>
                    <li class=\"list-inline-item\">
                      <a href=\"#\" class=\"link-one\">
                        <i class=\"fa fa-twitter\" aria-hidden=\"true\"></i>
                      </a>
                    </li>
                    <li class=\"list-inline-item\">
                      <a href=\"#\" class=\"link-one\">
                        <i class=\"fa fa-instagram\" aria-hidden=\"true\"></i>
                      </a>
                    </li>
                    <li class=\"list-inline-item\">
                      <a href=\"#\" class=\"link-one\">
                        <i class=\"fa fa-pinterest-p\" aria-hidden=\"true\"></i>
                      </a>
                    </li>
                    <li class=\"list-inline-item\">
                      <a href=\"#\" class=\"link-one\">
                        <i class=\"fa fa-dribbble\" aria-hidden=\"true\"></i>
                      </a>
                    </li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class=\"col-md-4\">
          <div class=\"card-box-d\">
            <div class=\"card-img-d\">
              <img src=\"/img/agent-1.jpg\" alt=\"\" class=\"img-d img-fluid\">
            </div>
            <div class=\"card-overlay card-overlay-hover\">
              <div class=\"card-header-d\">
                <div class=\"card-title-d align-self-center\">
                  <h3 class=\"title-d\">
                    <a href=\"agent-single.html\" class=\"link-two\">Stiven Spilver
                      <br> Darw</a>
                  </h3>
                </div>
              </div>
              <div class=\"card-body-d\">
                <p class=\"content-d color-text-a\">
                  Sed porttitor lectus nibh, Cras ultricies ligula sed magna dictum porta two.
                </p>
                <div class=\"info-agents color-a\">
                  <p>
                    <strong>Phone: </strong> +54 356 945234</p>
                  <p>
                    <strong>Email: </strong> agents@example.com</p>
                </div>
              </div>
              <div class=\"card-footer-d\">
                <div class=\"socials-footer d-flex justify-content-center\">
                  <ul class=\"list-inline\">
                    <li class=\"list-inline-item\">
                      <a href=\"#\" class=\"link-one\">
                        <i class=\"fa fa-facebook\" aria-hidden=\"true\"></i>
                      </a>
                    </li>
                    <li class=\"list-inline-item\">
                      <a href=\"#\" class=\"link-one\">
                        <i class=\"fa fa-twitter\" aria-hidden=\"true\"></i>
                      </a>
                    </li>
                    <li class=\"list-inline-item\">
                      <a href=\"#\" class=\"link-one\">
                        <i class=\"fa fa-instagram\" aria-hidden=\"true\"></i>
                      </a>
                    </li>
                    <li class=\"list-inline-item\">
                      <a href=\"#\" class=\"link-one\">
                        <i class=\"fa fa-pinterest-p\" aria-hidden=\"true\"></i>
                      </a>
                    </li>
                    <li class=\"list-inline-item\">
                      <a href=\"#\" class=\"link-one\">
                        <i class=\"fa fa-dribbble\" aria-hidden=\"true\"></i>
                      </a>
                    </li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class=\"col-md-4\">
          <div class=\"card-box-d\">
            <div class=\"card-img-d\">
              <img src=\"/img/agent-5.jpg\" alt=\"\" class=\"img-d img-fluid\">
            </div>
            <div class=\"card-overlay card-overlay-hover\">
              <div class=\"card-header-d\">
                <div class=\"card-title-d align-self-center\">
                  <h3 class=\"title-d\">
                    <a href=\"agent-single.html\" class=\"link-two\">Emma Toledo
                      <br> Cascada</a>
                  </h3>
                </div>
              </div>
              <div class=\"card-body-d\">
                <p class=\"content-d color-text-a\">
                  Sed porttitor lectus nibh, Cras ultricies ligula sed magna dictum porta two.
                </p>
                <div class=\"info-agents color-a\">
                  <p>
                    <strong>Phone: </strong> +54 356 945234</p>
                  <p>
                    <strong>Email: </strong> agents@example.com</p>
                </div>
              </div>
              <div class=\"card-footer-d\">
                <div class=\"socials-footer d-flex justify-content-center\">
                  <ul class=\"list-inline\">
                    <li class=\"list-inline-item\">
                      <a href=\"#\" class=\"link-one\">
                        <i class=\"fa fa-facebook\" aria-hidden=\"true\"></i>
                      </a>
                    </li>
                    <li class=\"list-inline-item\">
                      <a href=\"#\" class=\"link-one\">
                        <i class=\"fa fa-twitter\" aria-hidden=\"true\"></i>
                      </a>
                    </li>
                    <li class=\"list-inline-item\">
                      <a href=\"#\" class=\"link-one\">
                        <i class=\"fa fa-instagram\" aria-hidden=\"true\"></i>
                      </a>
                    </li>
                    <li class=\"list-inline-item\">
                      <a href=\"#\" class=\"link-one\">
                        <i class=\"fa fa-pinterest-p\" aria-hidden=\"true\"></i>
                      </a>
                    </li>
                    <li class=\"list-inline-item\">
                      <a href=\"#\" class=\"link-one\">
                        <i class=\"fa fa-dribbble\" aria-hidden=\"true\"></i>
                      </a>
                    </li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!--/ Agents End /-->

  <!--/ News Star /-->
  <section class=\"section-news section-t8\">
    <div class=\"container\">
      <div class=\"row\">
        <div class=\"col-md-12\">
          <div class=\"title-wrap d-flex justify-content-between\">
            <div class=\"title-box\">
              <h2 class=\"title-a\">Latest News</h2>
            </div>
            <div class=\"title-link\">
              <a href=\"blog-grid.html\">All News
                <span class=\"ion-ios-arrow-forward\"></span>
              </a>
            </div>
          </div>
        </div>
      </div>
      <div id=\"new-carousel\" class=\"owl-carousel owl-theme\">
        <div class=\"carousel-item-c\">
          <div class=\"card-box-b card-shadow news-box\">
            <div class=\"img-box-b\">
              <img src=\"/img/post-2.jpg\" alt=\"\" class=\"img-b img-fluid\">
            </div>
            <div class=\"card-overlay\">
              <div class=\"card-header-b\">
                <div class=\"card-category-b\">
                  <a href=\"#\" class=\"category-b\">House</a>
                </div>
                <div class=\"card-title-b\">
                  <h2 class=\"title-2\">
                    <a href=\"blog-single.html\">House is comming
                      <br> new</a>
                  </h2>
                </div>
                <div class=\"card-date\">
                  <span class=\"date-b\">18 Sep. 2017</span>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class=\"carousel-item-c\">
          <div class=\"card-box-b card-shadow news-box\">
            <div class=\"img-box-b\">
              <img src=\"/img/post-5.jpg\" alt=\"\" class=\"img-b img-fluid\">
            </div>
            <div class=\"card-overlay\">
              <div class=\"card-header-b\">
                <div class=\"card-category-b\">
                  <a href=\"#\" class=\"category-b\">Travel</a>
                </div>
                <div class=\"card-title-b\">
                  <h2 class=\"title-2\">
                    <a href=\"blog-single.html\">Travel is comming
                      <br> new</a>
                  </h2>
                </div>
                <div class=\"card-date\">
                  <span class=\"date-b\">18 Sep. 2017</span>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class=\"carousel-item-c\">
          <div class=\"card-box-b card-shadow news-box\">
            <div class=\"img-box-b\">
              <img src=\"/img/post-7.jpg\" alt=\"\" class=\"img-b img-fluid\">
            </div>
            <div class=\"card-overlay\">
              <div class=\"card-header-b\">
                <div class=\"card-category-b\">
                  <a href=\"#\" class=\"category-b\">Park</a>
                </div>
                <div class=\"card-title-b\">
                  <h2 class=\"title-2\">
                    <a href=\"blog-single.html\">Park is comming
                      <br> new</a>
                  </h2>
                </div>
                <div class=\"card-date\">
                  <span class=\"date-b\">18 Sep. 2017</span>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class=\"carousel-item-c\">
          <div class=\"card-box-b card-shadow news-box\">
            <div class=\"img-box-b\">
              <img src=\"/img/post-3.jpg\" alt=\"\" class=\"img-b img-fluid\">
            </div>
            <div class=\"card-overlay\">
              <div class=\"card-header-b\">
                <div class=\"card-category-b\">
                  <a href=\"#\" class=\"category-b\">Travel</a>
                </div>
                <div class=\"card-title-b\">
                  <h2 class=\"title-2\">
                    <a href=\"#\">Travel is comming
                      <br> new</a>
                  </h2>
                </div>
                <div class=\"card-date\">
                  <span class=\"date-b\">18 Sep. 2017</span>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!--/ News End /-->

  <!--/ Testimonials Star /-->
  <section class=\"section-testimonials section-t8 nav-arrow-a\">
    <div class=\"container\">
      <div class=\"row\">
        <div class=\"col-md-12\">
          <div class=\"title-wrap d-flex justify-content-between\">
            <div class=\"title-box\">
              <h2 class=\"title-a\">Testimonials</h2>
            </div>
          </div>
        </div>
      </div>
      <div id=\"testimonial-carousel\" class=\"owl-carousel owl-arrow\">
        <div class=\"carousel-item-a\">
          <div class=\"testimonials-box\">
            <div class=\"row\">
              <div class=\"col-sm-12 col-md-6\">
                <div class=\"testimonial-img\">
                  <img src=\"/img/testimonial-1.jpg\" alt=\"\" class=\"img-fluid\">
                </div>
              </div>
              <div class=\"col-sm-12 col-md-6\">
                <div class=\"testimonial-ico\">
                  <span class=\"ion-ios-quote\"></span>
                </div>
                <div class=\"testimonials-content\">
                  <p class=\"testimonial-text\">
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Omnis, cupiditate ea nam praesentium
                    debitis hic ber quibusdam
                    voluptatibus officia expedita corpori.
                  </p>
                </div>
                <div class=\"testimonial-author-box\">
                  <img src=\"/img/mini-testimonial-1.jpg\" alt=\"\" class=\"testimonial-avatar\">
                  <h5 class=\"testimonial-author\">Albert & Erika</h5>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class=\"carousel-item-a\">
          <div class=\"testimonials-box\">
            <div class=\"row\">
              <div class=\"col-sm-12 col-md-6\">
                <div class=\"testimonial-img\">
                  <img src=\"/img/testimonial-2.jpg\" alt=\"\" class=\"img-fluid\">
                </div>
              </div>
              <div class=\"col-sm-12 col-md-6\">
                <div class=\"testimonial-ico\">
                  <span class=\"ion-ios-quote\"></span>
                </div>
                <div class=\"testimonials-content\">
                  <p class=\"testimonial-text\">
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Omnis, cupiditate ea nam praesentium
                    debitis hic ber quibusdam
                    voluptatibus officia expedita corpori.
                  </p>
                </div>
                <div class=\"testimonial-author-box\">
                  <img src=\"/img/mini-testimonial-2.jpg\" alt=\"\" class=\"testimonial-avatar\">
                  <h5 class=\"testimonial-author\">Pablo & Emma</h5>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!--/ Testimonials End /-->
";
    }

    public function getTemplateName()
    {
        return "index.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  309 => 200,  294 => 191,  287 => 187,  280 => 183,  271 => 177,  260 => 169,  255 => 167,  245 => 160,  239 => 159,  227 => 149,  223 => 148,  109 => 36,  92 => 25,  87 => 23,  80 => 19,  75 => 17,  69 => 16,  59 => 8,  55 => 7,  50 => 4,  46 => 3,  35 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "index.twig", "/var/www/resources/templates/index.twig");
    }
}
