<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* index.twig */
class __TwigTemplate_1cff353b79a4ad57b419b0f569ca9554e2839b226cd25d3e4c107fbdb0506186 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'pageContent' => [$this, 'block_pageContent'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "layout.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $this->parent = $this->loadTemplate("layout.twig", "index.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_pageContent($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 4
        echo "  <!--/ Carousel Star /-->
  <div class=\"intro intro-carousel\">
    <div id=\"carousel\" class=\"owl-carousel owl-theme\">
      ";
        // line 7
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["residences"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["residence"]) {
            // line 8
            echo "      <div class=\"carousel-item-a intro-item bg-image\" style=\"background-image: url(/img/slide-";
            echo twig_escape_filter($this->env, twig_random($this->env, 1, 3), "html", null, true);
            echo ".jpg)\">
        <div class=\"overlay overlay-a\"></div>
        <div class=\"intro-content display-table\">
          <div class=\"table-cell\">
            <div class=\"container\">
              <div class=\"row\">
                <div class=\"col-lg-8\">
                  <div class=\"intro-body\">
                    <p class=\"intro-title-top\">";
            // line 16
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["residence"], "city", [], "any", false, false, false, 16), "html", null, true);
            echo ", ";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["residence"], "country", [], "any", false, false, false, 16), "html", null, true);
            echo "
                      <br> ";
            // line 17
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["residence"], "zip_code", [], "any", false, false, false, 17), "html", null, true);
            echo "</p>
                    <h1 class=\"intro-title mb-4\">
                      <span class=\"color-b\">";
            // line 19
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["residence"], "street_number", [], "any", false, false, false, 19), "html", null, true);
            echo " </span> ";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["residence"], "city", [], "any", false, false, false, 19), "html", null, true);
            echo "
                      <br> ";
            // line 20
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["residence"], "street", [], "any", false, false, false, 20), "html", null, true);
            if (twig_get_attribute($this->env, $this->source, $context["residence"], "street_bus", [], "any", false, false, false, 20)) {
                echo " <br>bus ";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["residence"], "street_bus", [], "any", false, false, false, 20), "html", null, true);
            }
            echo "</h1>
                    <p class=\"intro-subtitle intro-price\">
                      <form action=\"/residences/";
            // line 22
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["residence"], "id", [], "any", false, false, false, 22), "html", null, true);
            echo "\" method=\"GET\">
                        <input class=\"price-a\" type=\"submit\" value=\"";
            // line 23
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["residence"], "payment", [], "any", false, false, false, 23), "html", null, true);
            echo " | € ";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["residence"], "price", [], "any", false, false, false, 23), "html", null, true);
            echo "\"></input>
                      </form>
                    </p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['residence'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 34
        echo "    </div>
  </div>
  <!--/ Carousel end /-->

  <!--/ Property Star /-->
  <section class=\"section-property section-t8\">
    <div class=\"container\">
      <div class=\"row\">
        <div class=\"col-md-12\">
          <div class=\"title-wrap d-flex justify-content-between\">
            <div class=\"title-box\">
              <h2 class=\"title-a\">Latest Residences</h2>
            </div>
            <div class=\"title-link\">
              <a href=\"/residences/page/1\">All Residences
                <span class=\"ion-ios-arrow-forward\"></span>
              </a>
            </div>
          </div>
        </div>
      </div>
      <div id=\"property-carousel\" class=\"owl-carousel owl-theme\">
        ";
        // line 56
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["latest_properties"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["residence"]) {
            // line 57
            echo "        <div class=\"carousel-item-b\">
          <div class=\"card-box-a card-shadow\">
            <div class=\"img-box-a\">
              <img src=\"/img/property-";
            // line 60
            echo twig_escape_filter($this->env, twig_random($this->env, 1, 10), "html", null, true);
            echo ".jpg\" alt=\"\" class=\"img-a img-fluid\">
            </div>
            <div class=\"card-overlay\">
              <div class=\"card-overlay-a-content\">
                <div class=\"card-header-a\">
                  <h2 class=\"card-title-a\">
                    <a href=\"/residences/";
            // line 66
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["residence"], "id", [], "any", false, false, false, 66), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["residence"], "zip_code", [], "any", false, false, false, 66), "html", null, true);
            echo ", ";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["residence"], "city", [], "any", false, false, false, 66), "html", null, true);
            echo "
                    </a>
                    <p>
                      ";
            // line 69
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["residence"], "street", [], "any", false, false, false, 69), "html", null, true);
            echo " ";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["residence"], "street_number", [], "any", false, false, false, 69), "html", null, true);
            if (twig_get_attribute($this->env, $this->source, $context["residence"], "street_bus", [], "any", false, false, false, 69)) {
                echo " <br>bus ";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["residence"], "street_bus", [], "any", false, false, false, 69), "html", null, true);
            }
            // line 70
            echo "                    </p>
                  </h2>
                </div>
                <div class=\"card-body-a\">
                  <div class=\"price-box d-flex\">
                    <form action=\"/residences/";
            // line 75
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["residence"], "id", [], "any", false, false, false, 75), "html", null, true);
            echo "\" method=\"GET\">
                        <input class=\"price-a\" type=\"submit\" value=\"";
            // line 76
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["residence"], "payment", [], "any", false, false, false, 76), "html", null, true);
            echo " | € ";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["residence"], "price", [], "any", false, false, false, 76), "html", null, true);
            echo "\"></input>
                      </form>
                  </div>
                </div>
                <div class=\"card-footer-a\">
                  <ul class=\"card-info d-flex justify-content-around\">
                    <li>
                      <h4 class=\"card-info-title\">Area</h4>
                      <span>";
            // line 84
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["residence"], "surface", [], "any", false, false, false, 84), "html", null, true);
            echo "m
                        <sup>2</sup>
                      </span>
                    </li>
                    <li>
                      <h4 class=\"card-info-title\">Beds</h4>
                      <span>";
            // line 90
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["residence"], "bedrooms", [], "any", false, false, false, 90), "html", null, true);
            echo "</span>
                    </li>
                    <li>
                      <h4 class=\"card-info-title\">Baths</h4>
                      <span>";
            // line 94
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["residence"], "bathrooms", [], "any", false, false, false, 94), "html", null, true);
            echo "</span>
                    </li>
                    <li>
                      <h4 class=\"card-info-title\">Garages</h4>
                      <span>";
            // line 98
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["residence"], "garages", [], "any", false, false, false, 98), "html", null, true);
            echo "</span>
                    </li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
        </div>
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['residence'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 107
        echo "      </div>
    </div>
  </section>
  <!--/ Property End /-->

  <!--/ Agents Star /-->
  <section class=\"section-agents section-t8\">
    <div class=\"container\">
      <div class=\"row\">
        <div class=\"col-md-12\">
          <div class=\"title-wrap d-flex justify-content-between\">
            <div class=\"title-box\">
              <h2 class=\"title-a\">Our Agents</h2>
            </div>
            <div class=\"title-link\">
              <a href=\"/agents\">All Agents
                <span class=\"ion-ios-arrow-forward\"></span>
              </a>
            </div>
          </div>
        </div>
      </div>
      <div class=\"row\">
        ";
        // line 130
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["agents"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["agent"]) {
            // line 131
            echo "        <div class=\"col-md-4\">
          <div class=\"card-box-d\">
            <div class=\"card-img-d\">
              <img src=\"/uploadImg/";
            // line 134
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["agent"], "images", [], "any", false, false, false, 134), "html", null, true);
            echo "\" alt=\"\" class=\"img-d img-fluid\">
            </div>
            <div class=\"card-overlay card-overlay-hover\">
              <div class=\"card-header-d\">
                <div class=\"card-title-d align-self-center\">
                  <h3 class=\"title-d\">
                    ";
            // line 140
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["agent"], "user_name", [], "any", false, false, false, 140), "html", null, true);
            echo "
                  </h3>
                </div>
              </div>
              <div class=\"card-body-d\">
                <p class=\"content-d color-text-a\">
                  <a href=\"agency/";
            // line 146
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["agent"], "agency_id", [], "any", false, false, false, 146), "html", null, true);
            echo "\" class=\"link-two\">";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["agent"], "agency_name", [], "any", false, false, false, 146), "html", null, true);
            echo "</a>
                </p>
                <div class=\"info-agents color-a\">
                  <p>
                    <strong>Phone: </strong> ";
            // line 150
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["agent"], "phone_number", [], "any", false, false, false, 150), "html", null, true);
            echo "</p>
                  <p>
                    <strong>Email: </strong> ";
            // line 152
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["agent"], "email", [], "any", false, false, false, 152), "html", null, true);
            echo "</p>
                </div>
              </div>
              <div class=\"card-footer-d\">
                <div class=\"socials-footer d-flex justify-content-center\">
                  <ul class=\"list-inline\">
                    <li class=\"list-inline-item\">
                      <a href=\"#\" class=\"link-one\">
                        <i class=\"fa fa-facebook\" aria-hidden=\"true\"></i>
                      </a>
                    </li>
                    <li class=\"list-inline-item\">
                      <a href=\"#\" class=\"link-one\">
                        <i class=\"fa fa-twitter\" aria-hidden=\"true\"></i>
                      </a>
                    </li>
                    <li class=\"list-inline-item\">
                      <a href=\"#\" class=\"link-one\">
                        <i class=\"fa fa-instagram\" aria-hidden=\"true\"></i>
                      </a>
                    </li>
                    <li class=\"list-inline-item\">
                      <a href=\"#\" class=\"link-one\">
                        <i class=\"fa fa-pinterest-p\" aria-hidden=\"true\"></i>
                      </a>
                    </li>
                    <li class=\"list-inline-item\">
                      <a href=\"#\" class=\"link-one\">
                        <i class=\"fa fa-dribbble\" aria-hidden=\"true\"></i>
                      </a>
                    </li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
        </div>
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['agent'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 190
        echo "      </div>
    </div>
  </section>
  <!--/ Testimonials End /-->
";
    }

    public function getTemplateName()
    {
        return "index.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  352 => 190,  308 => 152,  303 => 150,  294 => 146,  285 => 140,  276 => 134,  271 => 131,  267 => 130,  242 => 107,  227 => 98,  220 => 94,  213 => 90,  204 => 84,  191 => 76,  187 => 75,  180 => 70,  172 => 69,  162 => 66,  153 => 60,  148 => 57,  144 => 56,  120 => 34,  101 => 23,  97 => 22,  88 => 20,  82 => 19,  77 => 17,  71 => 16,  59 => 8,  55 => 7,  50 => 4,  46 => 3,  35 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "index.twig", "/var/www/resources/templates/index.twig");
    }
}
