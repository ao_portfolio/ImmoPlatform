<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* layout.twig */
class __TwigTemplate_2416908ce864d8ce5a806fb5a83d9f20879437ef1c521bebb4ea9430e4a890d2 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
            'pageContent' => [$this, 'block_pageContent'],
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<!DOCTYPE html>
<html lang=\"en\">
<head>
  <meta charset=\"utf-8\">
  <title>EstateAgency Bootstrap Template</title>
  <meta content=\"width=device-width, initial-scale=1.0\" name=\"viewport\">
  <meta content=\"\" name=\"keywords\">
  <meta content=\"\" name=\"description\">

  <!-- Favicons -->
  <link href=\"/img/favicon.png\" rel=\"icon\">
  <link href=\"/img/apple-touch-icon.png\" rel=\"apple-touch-icon\">

  <!-- Google Fonts -->
  <link href=\"https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700\" rel=\"stylesheet\">

  <!-- Bootstrap CSS File -->
  <link href=\"/lib/bootstrap/css/bootstrap.min.css\" rel=\"stylesheet\">

  <!-- Libraries CSS Files -->
  <link href=\"/lib/font-awesome/css/font-awesome.min.css\" rel=\"stylesheet\">
  <link href=\"/lib/animate/animate.min.css\" rel=\"stylesheet\">
  <link href=\"/lib/ionicons/css/ionicons.min.css\" rel=\"stylesheet\">
  <link href=\"/lib/owlcarousel/assets/owl.carousel.min.css\" rel=\"stylesheet\">
  <link href=\"//cdn.datatables.net/1.11.3/css/jquery.dataTables.min.css\" rel=\"stylesheet\">

  <!-- Main Stylesheet File -->
  <link href=\"/css/style.css\" rel=\"stylesheet\">

  <!-- =======================================================
    Theme Name: EstateAgency
    Theme URL: https://bootstrapmade.com/real-estate-agency-bootstrap-template/
    Author: BootstrapMade.com
    License: https://bootstrapmade.com/license/
  ======================================================= -->
</head>

<body>

  <div class=\"click-closed\"></div>
  <!--/ Form Search Star /-->
  <div class=\"box-collapse\">
    <div class=\"title-box-d\">
      <h3 class=\"title-d\">Search Properties</h3>
    </div>
    <span class=\"close-box-collapse right-boxed ion-ios-close\"></span>
    <div class=\"box-collapse-wrap form\">
      <form class=\"form-a\" action=\"/residences/search/page/1\" method=\"GET\">
        <div class=\"row\">
          <div class=\"col-md-12 mb-2\">
            <div class=\"form-group\">
              <label for=\"keyword\">Keyword</label>
              <input type=\"text\" class=\"form-control form-control-lg form-control-a\" placeholder=\"Keyword\" name=\"keyword\">
            </div>
          </div>
          <div class=\"col-md-6 mb-2\">
            <div class=\"form-group\">
              <label for=\"type\">Type</label>
              <select class=\"form-control form-control-lg form-control-a\" id=\"type\" name=\"type\">
                <option>All Types</option>
                ";
        // line 61
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["types"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["type"]) {
            // line 62
            echo "                <option>";
            echo twig_escape_filter($this->env, $context["type"], "html", null, true);
            echo "</option>
                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['type'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 64
        echo "              </select>
            </div>
          </div>
          <div class=\"col-md-6 mb-2\">
            <div class=\"form-group\">
              <label for=\"city\">City</label>
              <select class=\"form-control form-control-lg form-control-a\" id=\"city\" name=\"city\">
                <option>All Cities</option>
                ";
        // line 72
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["cities"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["city"]) {
            // line 73
            echo "                <option>";
            echo twig_escape_filter($this->env, $context["city"], "html", null, true);
            echo "</option>
                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['city'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 75
        echo "              </select>
            </div>
          </div>
          <div class=\"col-md-6 mb-2\">
            <div class=\"form-group\">
              <label for=\"bedrooms\">Bedrooms</label>
              <input type=\"number\" class=\"form-control form-control-lg form-control-a\" id=\"bedrooms\" name=\"bedrooms\" value=\"1\" min=\"1\" max=\"100\">
            </div>
          </div>
          <div class=\"col-md-6 mb-2\">
            <div class=\"form-group\">
              <label for=\"bathrooms\">Bathrooms</label>
              <input type=\"number\" class=\"form-control form-control-lg form-control-a\" id=\"bathrooms\" name=\"bathrooms\" value=\"1\" min=\"1\" max=\"100\">
            </div>
          </div>
          <div class=\"col-md-6 mb-2\">
            <div class=\"form-group\">
              <label for=\"garages\">Garages</label>
              <input type=\"number\" class=\"form-control form-control-lg form-control-a\" id=\"garages\" name=\"garages\" value=\"0\" min=\"0\" max=\"100\">
            </div>
          </div>
          <div class=\"col-md-3 mb-2\">
            <div class=\"form-group\">
              <label for=\"min-price\">Min Price</label>
              <input type=\"number\" class=\"form-control form-control-lg form-control-a\" id=\"min-price\" name=\"min-price\" value=\"0\" min=\"0\" max=\"1000000000\">
            </div>
          </div>
          <div class=\"col-md-3 mb-2\">
            <div class=\"form-group\">
              <label for=\"max-price\">Max Price</label>
              <input type=\"number\" class=\"form-control form-control-lg form-control-a\" id=\"max-price\" name=\"max-price\" value=\"0\" min=\"0\" max=\"1000000000\">
            </div>
          </div>
          <div class=\"col-md-12\">
            <button type=\"submit\" class=\"btn btn-b\">Search Properties</button>
          </div>
        </div>
      </form>
    </div>
  </div>
  <!--/ Form Search End /-->

  ";
        // line 117
        $this->loadTemplate("partials/header.twig", "layout.twig", 117)->display($context);
        // line 118
        echo "
   ";
        // line 119
        $this->displayBlock('pageContent', $context, $blocks);
        // line 121
        echo "
  ";
        // line 122
        $this->loadTemplate("partials/footer.twig", "layout.twig", 122)->display($context);
        // line 123
        echo "
  <a href=\"#\" class=\"back-to-top\"><i class=\"fa fa-chevron-up\"></i></a>
  <div id=\"preloader\"></div>

  ";
        // line 127
        $this->loadTemplate("partials/libraries.twig", "layout.twig", 127)->display($context);
        // line 128
        echo "
</body>
</html>
";
    }

    // line 119
    public function block_pageContent($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 120
        echo "   ";
    }

    public function getTemplateName()
    {
        return "layout.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  211 => 120,  207 => 119,  200 => 128,  198 => 127,  192 => 123,  190 => 122,  187 => 121,  185 => 119,  182 => 118,  180 => 117,  136 => 75,  127 => 73,  123 => 72,  113 => 64,  104 => 62,  100 => 61,  38 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "layout.twig", "/var/www/resources/templates/layout.twig");
    }
}
