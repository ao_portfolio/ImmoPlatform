<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* addResidence.twig */
class __TwigTemplate_623405620eda103f3053162039aae2a3a1f1692a68bc998f739ddb04714d498f extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'pageContent' => [$this, 'block_pageContent'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "layout.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $this->parent = $this->loadTemplate("layout.twig", "addResidence.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_pageContent($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 4
        echo "<section class=\"section-services section-t8\">
   <div class=\"container\">
   ";
        // line 6
        $this->loadTemplate("partials/errors.twig", "addResidence.twig", 6)->display($context);
        // line 7
        echo "      <form action=\"addResidence.php\" method=\"post\" enctype=\"multipart/form-data\">
         <dt><label for=\"image\">Images</label></dt>
         <dt><input type=\"file\" id=\"images\" name=\"images[]\" value=\"\" multiple/><label for=\"images\"></label> ";
        // line 9
        if (twig_in_filter("images", ($context["formErrors"] ?? null))) {
            echo " image error ";
        }
        echo "</dt>

         <dt><label for=\"type\">Type</label></dt>
         <dt>
            <select id=\"type\" name=\"type\">
               ";
        // line 14
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["types"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["type"]) {
            // line 15
            echo "               <option value=\"";
            echo twig_escape_filter($this->env, $context["type"], "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, twig_capitalize_string_filter($this->env, $context["type"]), "html", null, true);
            echo " </option>
               ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['type'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 17
        echo "            </select>
            <label for=\"type\"><?php echo \$msgDescription ?></label>
         </dt>

         <dt><label for=\"price\">Price</label></dt>
         <dt><input type=\"number\" id=\"price\" name=\"price\" value=\"";
        // line 22
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["values"] ?? null), "price", [], "any", false, false, false, 22), "html", null, true);
        echo "\"><label for=\"price\"><?php echo \$msgDescription ?></label></dt>

         <div class=\"row\">
            <div class=\"col-md-4\">
               <dt><label for=\"country\">Country</label></dt>
               <dt><input type=\"text\" id=\"country\" name=\"country\" value=\"";
        // line 27
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["values"] ?? null), "country", [], "any", false, false, false, 27), "html", null, true);
        echo "\"><label for=\"country\"><?php echo \$msgDescription ?></label></dt>
            </div>
            <div class=\"col-md-4\">
               <dt><label for=\"city\">City</label></dt>
               <dt><input type=\"text\" id=\"city\" name=\"city\" value=\"";
        // line 31
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["values"] ?? null), "city", [], "any", false, false, false, 31), "html", null, true);
        echo "\"><label for=\"city\"><?php echo \$msgDescription ?></label></dt>
            </div>
            <div class=\"col-md-4\">
               <dt><label for=\"zip\">Zip code</label></dt>
               <dt><input type=\"text\" id=\"zip\" name=\"zip\" value=\"";
        // line 35
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["values"] ?? null), "zip", [], "any", false, false, false, 35), "html", null, true);
        echo "\"><label for=\"zip\"><?php echo \$msgDescription ?></label></dt>
            </div>
         </div>

         <dt><label for=\"name\">Name</label></dt>
         <dt><input type=\"text\" id=\"name\" name=\"name\" value=\"";
        // line 40
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["values"] ?? null), "name", [], "any", false, false, false, 40), "html", null, true);
        echo "\"><label for=\"name\"><?php echo \$msgDescription ?></label></dt>

         <dt><label for=\"description\">Description</label></dt>
         <dt><textarea type=\"text\" id=\"description\" name=\"description\" rows=\"4\" cols=\"50\">";
        // line 43
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["values"] ?? null), "description", [], "any", false, false, false, 43), "html", null, true);
        echo "</textarea><label for=\"description\"><?php echo \$msgDescription ?></label></dt>

         <dt>
               <input type=\"hidden\" name=\"moduleAction\" value=\"processUpload\" />
               <input type=\"submit\" name=\"btnSubmit\" value=\"Add\" />
         </dt>
      </form>
   </div>
</section>
";
    }

    public function getTemplateName()
    {
        return "addResidence.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  128 => 43,  122 => 40,  114 => 35,  107 => 31,  100 => 27,  92 => 22,  85 => 17,  74 => 15,  70 => 14,  60 => 9,  56 => 7,  54 => 6,  50 => 4,  46 => 3,  35 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "addResidence.twig", "/var/www/resources/templates/addResidence.twig");
    }
}
