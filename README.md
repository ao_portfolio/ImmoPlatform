# 2122PWMSS-SuperWaffle

## Name
Immowebsite

## Description
This is a website where you can search for houses when you're in the market for buying one. You can also login to save houses you like or add an alert if a house with 
certain properties enters the site. But real estate agencies can also have accounts to add houses.

## Installation
When you have imported the project (and have docker installed), you have to open a terminal and type the following command in the projectfolder:   
``` docker-compose up ```  
After this you need to login into the php-web container, to do this you first need to know the container id. You can find this with the folowing command:  
``` docker ps ```  
Then to login into the container you need to type the following, but change 'id' to the container id you found with the previous command.    
``` docker exec -it 'id' bash ```  
When you have done this you only need to type in one more command:  
``` composer install ```  
This should be it, after it is done installing you can type 'exit', close the terminal and enjoy our project.  

## Authors and acknowledgment
Team members: Aaron Odent, Jonathan Vercammen and Margaux Meuleman.

## Project status
Full in progress, but ready with milestone 1 and 2.

